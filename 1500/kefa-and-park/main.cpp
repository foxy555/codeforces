#include "bits/stdc++.h"

#define ll long long

using namespace std;

int n, m;
map<int, vector<int>> tree;
vector<int> nlist;
int ans = 0;
vector<vector<int>> treedata(1e5 + 5);
vector<vector<int>> rtreedata(1e5 + 5);
void dfs(int node, int k)
{
    if (nlist[node])
        ++k;
    else
        k = 0;

    if (k > m)
        return;

    if (tree[node].size() == 0)
    {
        ++ans;
        return;
    }
    for (int i = 0; i < tree[node].size(); ++i)
    {
        dfs(tree[node][i], k);
    }
}

void construct_tree(int node)
{
    for (int i = 0; i < tree[node].size(); ++i)
    {
        for (int j = 0; j < treedata[tree[node][i]].size(); ++j)
        {
            if (treedata[tree[node][i]][j] != node)
            {
                tree[tree[node][i]].push_back(treedata[tree[node][i]][j]);
            }
        }
        for (int j = 0; j < rtreedata[tree[node][i]].size(); ++j)
        {
            if (rtreedata[tree[node][i]][j] != node)
            {
                tree[tree[node][i]].push_back(rtreedata[tree[node][i]][j]);
            }
        }
        construct_tree(tree[node][i]);
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n >> m;

    for (int i = 0; i < n; ++i)
    {
        int a;
        cin >> a;
        nlist.push_back(a);
    }
    for (int i = 1; i < n; ++i)
    {
        int a, b;
        cin >> a >> b;
        if (a > b)
            swap(a, b);
        treedata[a - 1].push_back(b - 1);
        rtreedata[b - 1].push_back(a - 1);
    }
    tree[0] = treedata[0];
    construct_tree(0);
    /*
    for (auto i : tree)
    {
        cout << i.first << " ";
        for (auto j : i.second)
        {
            cout << j << " ";
        }
        cout << endl;
    }
    */
    dfs(0, 0);
    cout << ans << endl;
}
