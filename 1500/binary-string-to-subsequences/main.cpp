#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<bool> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            char a;
            cin >> a;
            nlist[i] = a - '0';
        }

        vector<pair<bool, int>> parts;
        parts.push_back({nlist.front(), 0});
        for (int i = 0; i < n; ++i)
        {
            if (parts.back().first == nlist[i])
            {
                ++parts.back().second;
            }
            else
            {
                parts.push_back({nlist[i], 1});
            }
        }
        set<int> indices;
        unordered_map<int, pair<int, pair<bool, int>>> p;
        // vector<pair<int, pair<bool, int>>> p(2e5 + 5);
        set<int> zero, one;
        vector<int> answer_cache(parts.size());

        for (int i = 0; i < parts.size(); ++i)
        {
            if (parts[i].first == 0)
            {
                zero.insert(i);
            }
            else
            {
                one.insert(i);
            }
            p[i] = {i, {parts[i]}};
            indices.insert(i);
        }
        answer_cache[0] = 0;
        for (int i = 1; i < parts.size(); ++i)
        {
            answer_cache[i] = answer_cache[i - 1] + parts[i - 1].second;
        }

        vector<int> ans(n);
        int a = 1;
        while (indices.size())
        {
            vector<int> to_remove;
            vector<int> selected;
            int max_take = 1e9;
            bool started = 0;
            bool val = 0;
            {
                int i = *indices.begin();
                while (true)
                {
                    if (started)
                    {
                        if (val == p[i].second.first)
                        {
                            selected.push_back(i);
                            val = !val;
                            max_take = min(max_take, p[i].second.second);
                        }
                    }
                    else
                    {
                        // start
                        started = 1;
                        val = !p[i].second.first;
                        selected.push_back(i);
                        max_take = min(max_take, p[i].second.second);
                    }
                    if (val)
                    {
                        // one
                        auto it = one.upper_bound(i);
                        if (it == one.end())
                            break;
                        i = *it;
                    }
                    else
                    {
                        // zero
                        auto it = zero.upper_bound(i);
                        if (it == zero.end())
                            break;
                        i = *it;
                    }
                }
            }
            // answer
            for (int i = 0; i < selected.size(); ++i)
            {
                ans[answer_cache[p[selected[i]].first]++] = a;
                p[selected[i]].second.second -= max_take;
                if (p[selected[i]].second.second == 0)
                    to_remove.push_back(selected[i]);
            }
            ++a;
            // erase
            for (int i = 0; i < to_remove.size(); ++i)
            {
                indices.erase(to_remove[i]);
                one.erase(to_remove[i]);
                zero.erase(to_remove[i]);
            }
            for (int j = 1; j < max_take; ++j)
            {
                for (int i = 0; i < selected.size(); ++i)
                {
                    ans[answer_cache[p[selected[i]].first]++] = a;
                    if (p[selected[i]].second.second == 0)
                        to_remove.push_back(selected[i]);
                }
                ++a;
            }
        }
        cout << a - 1 << endl;
        for (int i = 0; i < n; ++i)
        {
            cout << ans[i] << " ";
        }
        cout << endl;
    }
}
