#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int nlist[100005];
    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        for (int i = 0; i < n; ++i) {
            int a;
            cin >> a;
            nlist[a] = i;
        }
        vector<int> sim(n, 1);
        map<int, int> valcnt;
        valcnt[1] = n;
        bool ok = 1;
        for (int i = 1; i <= n; ++i) {
            if (sim[nlist[i]] != valcnt.rbegin()->first) {
                // cout << valcnt.rbegin()->first << endl;
                ok = 0;
                break;
            } else {
                if (--valcnt[valcnt.rbegin()->first] == 0) {
                    valcnt.erase(--valcnt.end());
                }
                if (nlist[i] < sim.size() - 1 && sim[nlist[i] + 1] != 0) {
                    sim[nlist[i] + 1] += sim[nlist[i]];
                    ++valcnt[sim[nlist[i] + 1]];
                }
                sim[nlist[i]] = 0;
            }
        }
        if (ok) {
            cout << "Yes" << endl;
        } else {
            cout << "No" << endl;
        }
    }
}
