#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, a, b, k;
    cin >> n >> a >> b >> k;

    vector<int> nlist(n);
    for (int &i : nlist)
    {
        cin >> i;
    }

    vector<int> anslist;
    int ans = 0;
    for (int i = 0; i < n; ++i)
    {
        if (a >= nlist[i])
        {
            ++ans;
            // cout << nlist[i] << endl;
            continue;
        }
        if (a + b < nlist[i] && nlist[i] % (a + b) <= a &&
            nlist[i] % (a + b) != 0)
        {
            ++ans;
            // cout << nlist[i] << endl;
            continue;
        }
        if (a + b <= nlist[i])
        {
            int left = nlist[i] % (a + b);
            if (left == 0)
            {
                left = b;
            }
            else
                left -= a;
            anslist.push_back(ceil(left / (double)a));
        }
        else
        {
            int left = nlist[i] - a;
            anslist.push_back(ceil(left / (double)a));
        }
    }
    sort(anslist.begin(), anslist.end());
    for (int i : anslist)
    {
        if (k >= i)
        {
            k -= i;
            ++ans;
        }
    }
    cout << ans << endl;
}
