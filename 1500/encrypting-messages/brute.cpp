#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, c;
    cin >> n >> m >> c;

    vector<int> nlist(n), mlist(m);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    for (int i = 0; i < m; ++i)
    {
        cin >> mlist[i];
    }

    for (int i = 0; i <= n - m; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            nlist[i + j] += mlist[j];
            nlist[i + j] %= c;
        }
    }
    for (auto i : nlist)
    {
        cout << i << " ";
    }
}

