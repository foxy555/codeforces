#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist(n - 1);
    for (int i = 0; i < n - 1; ++i) {
        cin >> nlist[i];
    }

    vector<int> intended_permutation(n, 1);
    vector<int> ans(n, 0);
    int small = 0, big = 0;
    for (int i = 1; i < n; ++i) {
        intended_permutation[i] = i + 1;
        ans[i] = ans[i - 1] + nlist[i - 1];
        small = min(small, ans[i]);
        big = max(big, ans[i]);
    }
    if (big < n) {
        for (int i = 0; i < n; ++i) {
            ans[i] += n - big;
        }
    } else if (small < 1) {
        for (int i = 0; i < n; ++i) {
            ans[i] += 1 - small;
        }
    }
    vector<int> sortedans = ans;
    sort(sortedans.begin(), sortedans.end());
    if (sortedans != intended_permutation) {
        cout << -1;
    } else {
        for (int i : ans) {
            cout << i << " ";
        }
    }
}
