#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, k;
        string s;
        cin >> n >> k >> s;
        vector<char> klist(k, '?');
        bool ok = 1;
        for (int i = 0; i < k; ++i) {
            for (int j = 0; i + j < n; j += k) {
                if (klist[i] == '?') {
                    if (s[i + j] != '?') klist[i] = s[i + j];
                } else {
                    if (s[i + j] != '?') {
                        if (s[i + j] != klist[i]) {
                            ok = 0;
                            break;
                        }
                    }
                }
            }
            if (!ok) {
                break;
            }
        }
        if (!ok) {
            cout << "NO" << endl;
            continue;
        }
        int one = 0, zero = 0, empty = 0;
        for (int i = 0; i < k; ++i) {
            if (klist[i] == '1') ++one;
            if (klist[i] == '0') ++zero;
            if (klist[i] == '?') ++empty;
        }

        if (one > k / 2 || zero > k / 2) {
            cout << "NO" << endl;
        } else
            cout << "YES" << endl;
    }
}
