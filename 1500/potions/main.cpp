#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<pair<ll, bool>> nlist(n);

    vector<pair<ll, ll>> negative;
    ll ans = 0;
    for (int i = 0; i < n; ++i) {
        cin >> nlist[i].first;
        if (nlist[i].first >= 0) {
            ++ans;
            nlist[i].second = 1;
        } else {
            nlist[i].second = 0;
            negative.push_back({nlist[i].first, i});
        }
    }
    sort(negative.rbegin(), negative.rend());
    for (int i = 0; i < negative.size(); ++i) {
        ll temp = 0;
        bool ok = 1;
        for (int j = 0; j < n; ++j) {
            if (nlist[j].second || j == negative[i].second) {
                temp += nlist[j].first;
                if (temp < 0) {
                    ok = 0;
                    break;
                }
            }
        }
        if (temp >= negative[i].first && ok) {
            ++ans;
            nlist[negative[i].second].second = 1;
        }
    }
    cout << ans << endl;
}

