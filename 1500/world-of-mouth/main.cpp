#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    string a, b;
    cin >> a >> b;
    int common = 0;
    for (int i = 0; i < min(a.size(), b.size()); ++i)
    {
        if (a[i] == b[i])
            common++;
        else
            break;
    }
    int steps = a.length() - common;
    steps += b.length() - common;
    if (steps <= n)
    {
        cout << "YES";
    }
    else
        cout << "NO";
}
