#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    ll sum = 0, high = 0;
    while (n--)
    {
        ll a;
        cin >> a;
        sum += a;
        high = max(high, a);
    }
    if (sum % 2 == 1 || 2 * high > sum)
    {
        cout << "NO";
    }
    else
        cout << "YES";
}

