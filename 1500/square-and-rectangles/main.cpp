#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<pair<pair<ll, ll>, pair<ll, ll>>> nlist(n);

    for (ll i = 0; i < n; ++i)
    {
        cin >> nlist[i].first.first >> nlist[i].second.first >>
            nlist[i].first.second >> nlist[i].second.second;
    }
    ll bigx, bigy;
    bigx = bigy = 0;
    ll minx, miny;
    minx = miny = INT_MAX;
    for (ll i = 0; i < n; ++i)
    {
        bigx = max(bigx, nlist[i].first.second);
        bigy = max(bigy, nlist[i].second.second);
        minx = min(minx, nlist[i].first.first);
        miny = min(miny, nlist[i].second.first);
    }
    if ((bigx - minx) != (bigy - miny))
    {
        cout << "NO";
        return 0;
    }
    ll area = (bigx - minx) * (bigy - miny);
    ll check = 0;
    for (ll i = 0; i < n; ++i)
    {
        check += (nlist[i].first.second - nlist[i].first.first) *
                 (nlist[i].second.second - nlist[i].second.first);
    }
    if (check == area)
    {
        cout << "YES";
    }
    else
        cout << "NO";
}
