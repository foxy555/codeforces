#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    ll factorial = 1;
    ll mod = 1e9 + 7;
    for (int i = 2; i <= n; ++i) {
        factorial *= i;
        factorial %= mod;
    }
    ll power = 1;
    for (int i = 1; i < n; ++i) {
        power *= 2;
        power %= mod;
    }
    ll ans = factorial - power;
    if (factorial < power) {
        ans += mod;
    }
    cout << ans % mod << endl;
}
