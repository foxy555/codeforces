#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, d;
    cin >> n >> d;

    vector<pair<ll, ll>> nlist(n);
    for (auto &i : nlist)
    {
        cin >> i.first >> i.second;
    }
    sort(nlist.begin(), nlist.end());
    ll head = 0, tail = 0;
    ll ans = 0, fri = 0;
    while (tail < n)
    {
        if (nlist[tail].first - nlist[head].first >= d)
        {
            fri = fri - nlist[head].second;
            ++head;
        }
        else
        {
            fri += nlist[tail].second;
            ++tail;
            ans = max(ans, fri);
        }
    }
    cout << ans << endl;
}
