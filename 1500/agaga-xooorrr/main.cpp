#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    int two[31];
    for (int i = 0; i < 31; ++i) {
        two[i] = pow(2, i);
    }

    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n);
        int allxor = 0;
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
            allxor ^= nlist[i];
        }
        if (allxor == 0) {
            cout << "YES" << endl;
        } else {
            int temp = 0;
            int cnt = 0;
            for (int i = 0; i < n; ++i) {
                temp ^= nlist[i];
                if (temp == allxor) {
                    ++cnt;
                    temp = 0;
                }
            }
            if (cnt > 2) {
                cout << "YES" << endl;
            } else {
                cout << "NO" << endl;
            }
        }
    }
}
