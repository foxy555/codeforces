#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> nlist(n);
    for (ll i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    vector<pair<ll, ll>> freqlist;
    sort(nlist.begin(), nlist.end());
    freqlist.push_back(make_pair(1, nlist.front()));

    for (ll i = 1; i < n; ++i)
    {
        if (nlist[i] == freqlist.back().second)
        {
            freqlist.back().first++;
        }
        else
            freqlist.push_back(make_pair(1, nlist[i]));
    }

    vector<pair<ll, int>> sumlist(freqlist.size());

    for (int i = 0; i < freqlist.size(); ++i)
    {
        sumlist[i].first = freqlist[i].first * freqlist[i].second;
        sumlist[i].second = i;
    }
    sort(sumlist.rbegin(), sumlist.rend());
    vector<bool> taken(freqlist.size() + 2, 0);

    ll ans = 0;
    ll ans1, ans2;
    ans1 = ans2 = 0;
    for (int i = 0; i < freqlist.size(); ++i)
    {
        if (i % 2)
            ans1 += freqlist[i].second * freqlist[i].first;
        else
            ans2 += freqlist[i].second * freqlist[i].first;
        if (taken[sumlist[i].second] || taken[sumlist[i].second + 2])
        {
            continue;
        }
        else
        {
            ans += sumlist[i].first;
            taken[sumlist[i].second + 1] = 1;
        }
    }
    cout << max({ans, ans1, ans2}) << endl;
}
