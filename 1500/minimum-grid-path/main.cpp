#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n);
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
        }

        vector<ll> hor, ver;
        vector<ll> horps, verps;
        for (int i = 0; i < n; ++i) {
            if (i % 2 == 0) {
                hor.push_back(nlist[i]);
            } else {
                ver.push_back(nlist[i]);
            }
        }
        horps.push_back(hor.front());
        verps.push_back(ver.front());
        for (int i = 1; i < hor.size(); ++i) {
            horps.push_back(horps.back() + hor[i]);
        }
        for (int i = 1; i < ver.size(); ++i) {
            verps.push_back(verps.back() + ver[i]);
        }
        ll ans = hor.front() * n + ver.front() * n;
        ll hormin = hor.front();
        ll vermin = ver.front();
        ll prevmax = n;
        ll curmax = n - 1;
        ll prevversum = ver.front() * n;
        ll prevhorsum = hor.front() * n;
        for (int i = 1; i < hor.size(); ++i) {
            // first hor then ver

            if (hor[i] < hormin) {
                hormin = hor[i];
                prevhorsum = horps[i] - hor[i] + hor[i] * curmax;
            } else {
                prevhorsum -= hormin;
                prevhorsum += hor[i];
            }
            ans = min(ans, prevhorsum + prevversum);
            /*
            currentcost -= hor[i - 1] * curmax;
            currentcost += hor[i] * curmax;
            ans = min(ans, currentcost);
            */
            // ver

            if (i == ver.size()) {
                break;
            }
            if (ver[i] < vermin) {
                vermin = ver[i];
                prevversum = verps[i] - ver[i] + ver[i] * curmax;
            } else {
                prevversum -= vermin;
                prevversum += ver[i];
            }
            ans = min(ans, prevversum + prevhorsum);

            /*
            currentcost -= ver[i - 1] * curmax;
            currentcost += ver[i] * curmax;
            ans = min(ans, currentcost);
            */
            --prevmax;
            --curmax;
        }
        cout << ans << endl;
    }
}
