#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, k;
    cin >> n >> k;

    vector<ll> nlist(n);
    for (ll i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    ll gains;
    cin >> gains;
    vector<ll> plist(n);
    for (ll i = 0; i < n; ++i)
    {
        cin >> plist[i];
    }

    multiset<ll> cheap;
    ll ans = 0;
    for (ll i = 0; i < n; ++i)
    {
        cheap.insert(plist[i]);
        if (k < nlist[i])
        {
            if (cheap.size() * gains + k < nlist[i])
            {
                cout << -1 << endl;
                return 0;
            }
            ll take = ceil((nlist[i] - k) / (double)gains);
            for (ll i = 0; i < take; ++i)
            {
                ans += *cheap.begin();
                cheap.erase(cheap.begin());
            }
            k += take * gains;
        }
    }
    cout << ans << endl;
}
