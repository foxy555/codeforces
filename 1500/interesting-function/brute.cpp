#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int l, r;
        cin >> l >> r;

        string oldl = to_string(l);
        int ans = 0;
        while (l != r) {
            ++l;
            string newl = to_string(l);
            if (newl.size() != oldl.size()) {
                ++ans;
                for (int i = 0; i < oldl.size(); ++i) {
                    if (newl[i + 1] != oldl[i]) {
                        ++ans;
                    }
                }
            } else {
                for (int i = 0; i < oldl.size(); ++i) {
                    if (newl[i] != oldl[i]) {
                        ++ans;
                    }
                }
            }
            oldl = newl;
        }
        cout << ans << endl;
    }
}
