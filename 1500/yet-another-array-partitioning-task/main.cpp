#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, m, k;
    cin >> n >> m >> k;

    vector<pair<int, bool>> nlist(n);
    map<int, set<int>> sortedlist;
    for (int i = 0; i < n; ++i) {
        cin >> nlist[i].first;
        sortedlist[nlist[i].first].insert(i);
        nlist[i].second = 0;
    }
    ll temp = m * k;
    ll ans = 0;
    for (auto it = sortedlist.rbegin(); it != sortedlist.rend(); ++it) {
        while (it->second.size() && temp) {
            nlist[*(it->second.begin())].second = 1;
            ans += it->first;
            it->second.erase(it->second.begin());
            --temp;
        }
    }
    int cnt = 0;
    int total = 0;
    cout << ans << endl;
    for (int i = 0; i < n; ++i) {
        cnt += nlist[i].second;
        if (cnt == m) {
            cout << i + 1 << " ";
            cnt = 0;
            ++total;
        }
        if (total == k - 1) break;
    }
}
