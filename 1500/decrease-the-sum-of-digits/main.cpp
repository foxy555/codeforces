#include "bits/stdc++.h"

#define ll long long

using namespace std;

vector<int> nlist;
ll n;
int getSum() {
    int sum = 0;
    for (auto i : nlist) {
        sum += i;
    }

    return sum;
}

void getVector() {
    nlist.clear();
    ll n1 = n;
    while (n1 != 0) {
        nlist.push_back(n1 % 10);
        n1 /= 10;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        cin >> n;
        getVector();

        int s;
        cin >> s;

        ll ans = 0;
        bool ok = 0;
        for (int i = 0; i < nlist.size(); ++i) {
            int sum = getSum();
            if (sum <= s) {
                break;
            }
            if (nlist[i] == 0) continue;
            ll addition = (10 - nlist[i]) * pow(10, i);
            n += addition;
            getVector();
            ans += addition;
        }
        getVector();
        int sum = getSum();
        if (sum > s) {
            ans += (10 - nlist.back()) * pow(10, nlist.size() - 1);
        }

        cout << ans << endl;

        nlist.clear();
    }
}
