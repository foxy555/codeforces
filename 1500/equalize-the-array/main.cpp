#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;

        map<int, int> cnt;
        vector<int> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
            ++cnt[nlist[i]];
        }

        map<int, int> cmap;
        for (auto i : cnt)
        {
            ++cmap[i.second];
        }

        map<int, int> before;
        int temp = 0;
        for (auto i : cmap)
        {
            before[i.first] = temp + i.second * i.first;
            temp = before[i.first];
        }
        map<int, int> after;
        int sum = 0;

        for (auto i = next(cmap.rbegin()); i != cmap.rend(); ++i)
        {
            int delta = prev(i)->first - i->first;
            after[i->first] = after[prev(i)->first] + delta * prev(i)->second;
            after[i->first] += delta * sum;
            sum += prev(i)->second;
        }
        int ans = 1e9;
        for (auto i : before)
        {
            int price = 0;
            price += before[i.first] - i.first * cmap[i.first];
            price += after[i.first];

            ans = min(price, ans);
            // cout << ans << endl;
        }
        cout << ans << endl;
    }
}
