#include "bits/stdc++.h"

#define ll long long

using namespace std;
const int len = 2e5 + 5;
int parent[len];
int size[len];
// A cycle is created when a number is not the first number that gets added to
// the member list and it connects to something inside the member_list[parent]
set<int> member_list[len];
int member_list_last_insert[len];

bool cycle[len];

int find_parent(int n) {
    if (n == parent[n]) return n;
    return find_parent(parent[n]);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    for (int i = 0; i < len; ++i) {
        parent[i] = i;
        cycle[i] = 0;
        member_list_last_insert[i] = i;
    }

    while (m--) {
        int a, b;
        cin >> a >> b;

        int pa = find_parent(a);
        int pb = find_parent(b);
        if (pa != pb) {
            if (size[pa] < size[pb]) swap(pa, pb);
            parent[pb] = pa;
            size[pa] += size[pb];

        } else {
            // if a already exists in the list
            if (member_list[pa].count(a)) {
                if (member_list_last_insert[pa] != a) cycle[pa] = 1;
            }
            if (member_list[pa].count(b)) {
                if (member_list_last_insert[pa] != b) cycle[pa] = 1;
            }
        }
        // insert new members into the list
    }
}
