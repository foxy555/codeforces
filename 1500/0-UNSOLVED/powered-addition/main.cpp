#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int &i : nlist)
        {
            cin >> i;
        }

        if (is_sorted(nlist.begin(), nlist.end()))
        {
            cout << 0 << endl;
            continue;
        }
    }
}
