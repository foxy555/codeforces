#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    cout << 4 + 3 * n << endl;
    cout << 0 << " " << 0 << endl;
    cout << 1 << " " << 0 << endl;
    cout << 1 << " " << 1 << endl;
    cout << 0 << " " << 1 << endl;
    for (int i = 0; i < n; ++i)
    {
        cout << i + 2 << " " << i + 2 << endl;
        cout << i + 1 << " " << i + 2 << endl;
        cout << i + 2 << " " << i + 1 << endl;
    }
}
