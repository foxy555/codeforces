#include "bits/stdc++.h"

#define ll long long

using namespace std;

string s;
int cnt;
char c;
void ir(int &r)
{
    if (r == s.size() - 1)
        return;
    ++r;
    if (s[r] == c)
        ++cnt;
}
void il(int &l)
{
    if (s[l] == c)
        --cnt;
    ++l;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k >> s;

    vector<int> anslist;
    {
        int l = 0, r = 0;
        cnt = 0;
        if (s[0] != 'a')
            ++cnt;
        c = 'b';
        while (true)
        {
            int length = r - l + 1;
            // cout << l << " " << r << " " << length << " " << cnt << endl;

            if (cnt > k)
            {
                anslist.push_back(0);
                if (l == r)
                    ir(r);
                else
                    il(l);
            }
            else
            {
                anslist.push_back(length);
                if (r == n - 1)
                    il(l);
                else
                    ir(r);
            }
            if (l == n - 1)
                break;
            if (l >= n || r >= n)
                break;
        }
    }
    {
        int l = 0, r = 0;
        cnt = 0;
        if (s[0] != 'b')
            ++cnt;
        c = 'a';
        while (true)
        {
            int length = r - l + 1;

            if (cnt > k)
            {
                anslist.push_back(0);
                if (l == r)
                    ir(r);
                else
                    il(l);
            }
            else
            {
                anslist.push_back(length);
                if (r == n - 1)
                    il(l);
                else
                    ir(r);
            }
            if (l == n - 1)
                break;
            if (l >= n || r >= n)
                break;
        }
    }
    cout << *max_element(anslist.begin(), anslist.end());
}

