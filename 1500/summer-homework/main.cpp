#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    vector<ll> fibonacci;
    {
        ll a = 1;
        ll b = 1;
        fibonacci.push_back(a);
        fibonacci.push_back(a);
        for (ll i = 0; i <= 100; ++i)
        {
            fibonacci.push_back((a + b) % (ll)1e9);
            ll temp = b;
            b = a + b;
            b %= (ll)1e9;
            a = temp % (ll)1e9;
        }
    }
    ll n, m;
    cin >> n >> m;

    vector<ll> nlist(n);
    for (ll i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    while (m--)
    {
        ll a, b, c;
        cin >> a >> b >> c;
        if (a == 1)
        {
            nlist[b - 1] = c;
        }
        else
        {
            ll ans = 0;
            for (ll i = 0; i < c - b + 1; ++i)
            {
                ans += (nlist[i + b - 1] * fibonacci[i]) % (ll)1e9;
            }
            cout << ans % (ll)1e9 << endl;
        }
    }
}
