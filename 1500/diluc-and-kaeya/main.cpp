#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        string s;
        cin >> n >> s;

        map<pair<int, int>, int> nmap;
        int d = 0, k = 0;
        for (int i = 0; i < n; ++i) {
            if (s[i] == 'D')
                ++d;
            else
                ++k;
            int gcd = __gcd(d, k);
            cout << ++nmap[{d / gcd, k / gcd}] << " ";
        }
        cout << endl;
    }
}
