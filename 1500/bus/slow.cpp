#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, m;
    cin >> n >> m;

    vector<pair<ll, ll>> nlist(n);

    for (ll i = 0; i < n; ++i)
    {
        cin >> nlist[i].first >> nlist[i].second;
    }
    ll ltime = 0;
    ll ans = 0;
    ll ppl = 0;
    ll d = 0;
    int laststudent = 0;
    map<ll, ll> stops;
    vector<ll> temp(1e5 + 69, -1969);
    for (ll i = 0; i < n; ++i)
    {
        stops[nlist[i].second]++;
        ppl++;
        if (nlist[i].first > ans)
            ans += nlist[i].first - ans;
        ltime = nlist[i].first;
        d = max(d, nlist[i].second);
        if (ppl == m || i == n - 1)
        {
            ll last = 0;
            for (auto j : stops)
            {
                ans += j.first - last;
                last = j.first;
                temp[j.first] = ans;
                ans += 1 + j.second / 2;
            }
            for (ll k = laststudent; k <= i; ++k)
            {
                if (temp[nlist[k].second] != -420)
                {
                    cout << temp[nlist[k].second] << " ";
                }
            }
            stops.clear();
            ans += d;
            d = 0;
            ppl = 0;
            laststudent = i + 1;
        }
    }
}

