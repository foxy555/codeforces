#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    cout << min(n, m) + 1 << endl;
    int x = 0;
    int y = m;
    while (y >= 0 && x <= n)
    {
        cout << x << " " << y << endl;
        --y;
        ++x;
    }
}
