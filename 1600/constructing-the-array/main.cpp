#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> ans(n, 0);
        set<pair<int, int>> zeroseq;
        // {zero count, beginning index}
        zeroseq.insert({n, n});
        for (int i = 0; i < n; ++i)
        {
            pair<int, int> greatest = *zeroseq.rbegin();
            zeroseq.erase(prev(zeroseq.end()));

            int l = n - greatest.second + 1;
            int r = n - greatest.second + greatest.first;
            int in = -1;
            if ((r - l + 1) % 2 == 0)
                in = (l + r - 1) / 2 - 1;
            else
                in = (l + r) / 2 - 1;
            ans[in] = i + 1;
            zeroseq.insert({in - (l - 1), n - l + 1});
            zeroseq.insert({r - 1 - in, n - (in + 1)});
        }
        for (int i = 0; i < n; ++i)
        {
            cout << ans[i] << " ";
        }
        cout << endl;
    }
}
