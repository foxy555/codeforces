#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> nlist(n);
    {
        ll sum = 0;
        for (ll i = 0; i < n; ++i)
        {
            cin >> nlist[i];
            sum += nlist[i];
        }
        if (sum % 360 == 0)
        {
            cout << "YES" << endl;
            return 0;
        }
    }
    vector<ll> directions(n, 1);
    ll poss = 0;
    for (ll i = 0; i < n; ++i)
    {
        do
        {
            ll test = 0;
            for (ll j = 0; j < n; ++j)
            {
                test += directions[j] * nlist[j];
            }
            if (test % 360 == 0)
            {
                cout << "YES" << endl;
                return 0;
            }
        } while (next_permutation(directions.begin(), directions.end()));
        directions[i] = -1;
    }
    cout << "NO" << endl;
}
