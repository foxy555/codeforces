#include "bits/stdc++.h"

using namespace std;

map<int, long long> fmap;

long long fib(int n)
{
    if (fmap[n] != 0)
        return fmap[n];
    if (n <= 2)
        return 1;
    fmap[n] = fib(n - 1) + fib(n - 2);
    return fmap[n];
}

long long fib_brute(int n)
{
    if (n <= 2)
        return 1;
    return fib(n - 1) + fib(n - 2);
}

map<pair<int, int>, long long> gmap;
long long grid_Traveler(int a, int b)
{
    if (gmap[{a, b}] != 0 || gmap[{b, a}] != 0)
        return max(gmap[{a, b}], gmap[{b, a}]);
    if (a == 0 || b == 0)
        return 0;
    if (a == 1 && b == 1)
        return 1;
    gmap[{a, b}] = grid_Traveler(a - 1, b) + grid_Traveler(a, b - 1);
    return gmap[{a, b}];
}

int main() { cout << grid_Traveler(18, 18) << endl; }
