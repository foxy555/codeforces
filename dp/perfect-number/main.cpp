#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int k;
    cin >> k;
    int t = 0;
    for (int i = 19; i < 100000000; ++i)
    {
        string s = to_string(i);
        int sum = 0;
        for (int j = 0; j < s.size(); ++j)
        {
            sum += s[j] - '0';
            if (sum > 10)
                break;
        }
        if (sum == 10)
        {
            ++t;
            if (t == k)
            {
                cout << i << endl;
                return 0;
            }
        }
    }
}
