#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    ll a, b, ans;
    a = b = ans = 0;
    for (int i = 0; i < s.length(); ++i)
    {
        if (s[i] == 'o')
        {
            b += a;
        }
        else
        {
            if (i != 0 && s[i - 1] == 'v')
            {
                a++;
                ans += b;
            }
        }
    }
    cout << ans << endl;
}
