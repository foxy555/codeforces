#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    ll ans = 0;
    for (ll i = 2; i < n; ++i)
    {
        ans += i * (i + 1);
    }
    cout << ans << endl;
}
