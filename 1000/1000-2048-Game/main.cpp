#include <iostream>
#include <vector>
using namespace std;
int main()
{
    int testcases;
    cin >> testcases;
    for(int t = 0; t < testcases; t++)
    {
        vector<int> numbers;
        int amount;
        cin >> amount;
        bool present = false;
        for(int a = 0; a < amount; a++)
        {
            int num;
            cin >> num;
            numbers.push_back(num);
            if(num == 2048)
                present = true;
        }
        if(present)
            cout << "YES\n";
        else
        {
            int sum = 0;
            for(int i = 0; i < numbers.size(); i++)
            {
                if(numbers[i] < 2048)
                    sum += numbers[i];
            }
            if(sum >= 2048)
                cout << "YES\n";
            else
                cout << "NO\n";
        }
        
    }
}
