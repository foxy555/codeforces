#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    int tc;
    cin >> tc;

    for(int t = 0; t < tc; t++)
    {
        string letters;
        cin >> letters;
        
        string uletters = "";

        for(int i = 0; i < letters.length(); i++)
        {
            bool unique = true;
            for(int a = 0; a < uletters.length(); a++)
            {
                if(letters[i] == uletters[a])
                {
                    unique = false;
                    break;
                }
            }
            if(unique)
                uletters = uletters + letters[i];

        }
        string goodletters = "";
        for(int i = 0; i < uletters.length(); i++)
        {
            bool good = false;
            for(int a = 0; a < letters.length() - 1; a++)
            {
                if(letters[a] == uletters[i] && letters[a + 1] == uletters[i])
                {
                    a++;
                }
                else if(letters[a] != uletters[i]) ;
                else
                {good = true;}
            }
            if(!good && letters[letters.length() - 1] == uletters[i] && letters[letters.length() - 2] != uletters[i])
            {
                good = !good;
            }
            if(good)
                goodletters = goodletters + uletters[i];
        }
        for(int i = 0; i < uletters.length(); i++)
        {
            int counter = 0;
            for(int a = 0; a < letters.length(); a++)
            {
                if(uletters[i] == letters[a])
                    counter++;
            }
            if(counter % 2 != 0 && goodletters.find(uletters[i]) == std::string::npos)
                goodletters = goodletters + uletters[i];
        }
        sort(goodletters.begin(), goodletters.end());
        cout << goodletters << endl;

    }
}
