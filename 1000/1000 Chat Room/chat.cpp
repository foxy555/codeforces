#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int main()
{
    string chat;
    cin >> chat;

    size_t hello[5] = {0,0,0,0,0};

    hello[0] = chat.find("h");

    hello[1] = chat.find("e", hello[0] + 1);
    
    hello[2] = chat.find("l", hello[1] + 1);

    hello[3] = chat.find("l", hello[2] + 1);

    hello[4] = chat.find("o", hello[3] + 1);
    for(int i = 0; i < 5; i++)
    {
        if(hello[i] == std::string::npos)
        {
            cout << "NO\n";
            return 0;
        }
        
    }
    size_t unsorted[5];
    copy(begin(hello), end(hello), begin(unsorted));
    sort(hello, hello + 4);
    if(equal(begin(hello), end(hello), begin(unsorted)))
        cout << "YES\n";
    else
        cout << "NO\n";
}