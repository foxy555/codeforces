#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, x;
        cin >> n >> x;
        int biggest = 0;
        bool ok = false;
        for (int i = 0; i < n; i++)
        {
            int a;
            cin >> a;
            biggest = max(biggest, a);
            if (a == x)
            {
                cout << 1 << endl;
                ok = true;
            }
        }
        if (ok)
            continue;
        if (biggest > x)
        {
            cout << 2 << endl;
        }
        else
        {
            cout << int(ceil(x / (double)biggest)) << endl;
        }
    }
}