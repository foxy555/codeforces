#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<string> board;
    vector<string> nboard;
    for (int i = 0; i < n; i++)
    {
        string s;
        string ns(m, '.');
        cin >> s;
        board.push_back(s);
        nboard.push_back(ns);
    }
    for (int i = 0; i < n - 2; i++)
    {
        for (int j = 0; j < m - 2; j++)
        {
            if (board[i][j] == '#' && board[i + 1][j] == '#' && board[i + 2][j] == '#' && board[i + 1][j + 2] == '#' && board[i][j + 1] == '#' && board[i][j + 2] == '#' && board[i + 2][j + 2] == '#' && board[i + 2][j + 1] == '#')
            {
                nboard[i][j] = '#';
                nboard[i + 1][j] = '#';
                nboard[i + 2][j] = '#';
                nboard[i + 2][j + 1] = '#';
                nboard[i + 2][j + 2] = '#';
                nboard[i][j + 1] = '#';
                nboard[i][j + 2] = '#';
                nboard[i + 1][j + 2] = '#';
            }
        }
    }
    if (nboard == board)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
}