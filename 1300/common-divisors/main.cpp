#include <bits/stdc++.h>

#define ll long long

using namespace std;
ll countDivisors(ll n)
{
    ll cnt = 0;
    for (ll i = 1; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            if (n / i == i)
                cnt++;

            else
                cnt = cnt + 2;
        }
    }
    return cnt;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> a(n);
    for (ll i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    ll gd = a[0];
    for (ll i = 1; i < n; i++)
    {
        gd = __gcd(a[i], gd);
        if (gd == 1)
        {
            break;
        }
    }
    ll ans = countDivisors(gd);
    cout << ans << endl;
}