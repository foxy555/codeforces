#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    ll k;
    cin >> n >> m >> k;

    if (k < n)
    {
        cout << k + 1 << " " << 1 << endl;
        return 0;
    }
    if (k < n + m - 1)
    {
        cout << n << " " << k - n + 2 << endl;
        return 0;
    }
    k -= -2 + n + m;
    m--;
    cout << n - (k - 1) / m - 1 << " ";
    if (((k - 1) / m) % 2)
    {
        cout << (k - 1) % m + 2 << endl;
    }
    else
    {
        cout << m - (k - 1) % m + 1 << endl;
    }
}