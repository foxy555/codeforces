#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    double average = 0;
    for (int i = 0; i < n; i++)
    {
        int counter = 1;
        double t = nlist[i];
        if (k == 1)
        {
            average = max(average, t);
        }
        for (int j = i + 1; j < n; j++)
        {
            t += nlist[j];
            counter++;
            if (counter >= k)
            {
                average = max(average, t / counter);
            }
        }
    }
    cout << setprecision(16) << average << endl;
}