#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int n, m, k;
    cin >> n >> m >> k;
    vector<string> room;
    vector<vector<int>> hspace;
    for (int i = 0; i < n; i++)
    {
        string s;
        cin >> s;
        vector<int> linespace;
        int cnt = 0;
        for (int j = 0; j < s.length(); j++)
        {
            if (s[j] == '.')
            {
                cnt++;
            }
            if (s[j] != '.' || j == s.length() - 1)
            {
                if (cnt >= k)
                    linespace.push_back(cnt);
                cnt = 0;
            }
        }
        hspace.push_back(linespace);
        room.push_back(s);
    }
    vector<vector<int>> vspace;
    for (int i = 0; i < m; i++)
    {
        vector<int> linespace;
        int cnt = 0;
        for (int j = 0; j < n; j++)
        {
            if (room[j][i] == '.')
            {
                cnt++;
            }
            if (room[j][i] != '.' || j == n - 1)
            {
                if (cnt >= k)
                    linespace.push_back(cnt);
                cnt = 0;
            }
        }
        vspace.push_back(linespace);
    }
    int ans = 0;
    for (int i = 0; i < hspace.size(); i++)
    {
        for (int j = 0; j < hspace[i].size(); j++)
        {
            if (hspace[i][j] >= k)
            {
                ans += hspace[i][j] - (k - 1);
            }
        }
    }
    if (k == 1)
    {
        cout << ans << endl;
        return 0;
    }
    for (int i = 0; i < vspace.size(); i++)
    {
        for (int j = 0; j < vspace[i].size(); j++)
        {
            if (vspace[i][j] >= k)
            {
                ans += vspace[i][j] - (k - 1);
            }
        }
    }
    cout << ans << endl;
}