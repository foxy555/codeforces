#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    queue<int> screen;
    set<int> data;
    int occupied = 0;
    for (int i = 0; i < n; i++)
    {
        int a;
        cin >> a;

        if (occupied != k)
        {
            if (!data.count(a))
            {
                screen.push(a);
                data.insert(a);
                occupied++;
            }
        }
        else
        {
            if (!data.count(a))
            {
                data.erase(screen.front());
                screen.pop();
                data.insert(a);
                screen.push(a);
            }
        }
    }
    cout << occupied << endl;
    vector<int> answer;
    while (!screen.empty())
    {
        answer.push_back(screen.front());
        screen.pop();
    }
    for (int i = occupied - 1; i >= 0; i--)
    {
        cout << answer[i] << " ";
    }
    cout << endl;
}