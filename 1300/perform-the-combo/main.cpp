#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, m;
        string s;
        cin >> n >> m >> s;
        vector<int> p(m);
        for (int i = 0; i < m; i++)
        {
            cin >> p[i];
        }
        vector<int> ans(30, 0);
        vector<int> aux(n, 1);
        sort(p.rbegin(), p.rend());
        int add = 0;
        int j = 0;
        for (int i = n - 1; i >= 0; i--)
        {
            while (i == p[j] - 1 && j < m)
            {
                add++;
                j++;
            }
            aux[i] += add;
            ans[int(s[i]) - 97] += aux[i];
        }
        for (int i = 0; i < 26; i++)
        {
            cout << ans[i] << " ";
        }

        cout << endl;
    }
}