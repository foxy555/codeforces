#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string s;
    cin >> s;
    int lc = 0;
    int rc = 0;
    int flp = INT_MAX;
    int frp = INT_MAX;
    int llp = -1;
    int lrp = -1;
    for (int i = 0; i < n; i++)
    {
        if (s[i] == 'L')
        {
            flp = min(flp, i);
            llp = max(llp, i);
            lc++;
        }
        if (s[i] == 'R')
        {
            frp = min(frp, i);
            lrp = max(lrp, i);
            rc++;
        }
    }
    if (rc == 0)
    {
        cout << llp + 1 << " " << flp << endl;
        return 0;
    }
    if (lc == 0)
    {
        cout << frp + 1 << " " << lrp + 2 << endl;
        return 0;
    }
    cout << frp + 1 << " " << flp << endl;
}