#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    map<ll, ll> nlist;
    for (ll i = 0; i < n; i++)
    {
        ll a;
        cin >> a;
        nlist[a]++;
    }
    ll m;
    cin >> m;
    vector<pair<ll, pair<ll, ll>>> movie(m, make_pair(0, make_pair(0, 0)));
    for (ll i = 0; i < m; i++)
    {
        ll a;
        cin >> a;
        movie[i].first = nlist[a];
        movie[i].second.second = i + 1;
    }
    for (ll i = 0; i < m; i++)
    {
        ll a;
        cin >> a;
        movie[i].second.first = nlist[a];
    }
    sort(movie.rbegin(), movie.rend());

    ll a = 0;
    ll ma = 0;
    ll pos = 1;
    while (movie[a].first == movie[0].first && a < movie.size())
    {
        ll t = movie[a].first + movie[a].second.first;
        if (t > ma)
            pos = movie[a].second.second;
        ma = max(t, ma);

        a++;
    }
    cout << pos << endl;
}