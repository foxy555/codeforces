#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    int r = 0, l = INT_MAX, u = INT_MAX, d = 0;
    int total = 0;
    for (int i = 0; i < n; i++)
    {
        string s;
        cin >> s;
        for (int j = 0; j < m; j++)
        {
            if (s[j] == 'B')
            {
                r = max(r, j);
                l = min(l, j);
                u = min(u, i);
                d = max(d, i);
                total++;
            }
        }
    }
    if (total == 0)
    {
        cout << 1 << endl;
        return 0;
    }
    int dim = max(abs(l - r) + 1, abs(u - d) + 1);
    if (m * n >= dim * dim && n >= dim && m >= dim)
    {
        cout << dim * dim - total << endl;
    }
    else
    {
        cout << -1 << endl;
    }
}