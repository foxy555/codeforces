#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<ll> money(n);

    for (int i = 0; i < m; i++)
    {
        int a, b, c;
        cin >> a >> b >> c;

        money[a - 1] -= c;
        money[b - 1] += c;
    }
    ll sum = 0;
    for (int i = 0; i < n; i++)
    {
        if (money[i] > 0)
            sum += money[i];
    }
    cout << sum << endl;
}