#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        vector<string> board(9);
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                char a;
                cin >> a;
                if (a == 49)
                    a++;
                board[i] += a;
            }
        }
        for (int i = 0; i < 9; i++)
        {
            cout << board[i] << endl;
        }
    }
}