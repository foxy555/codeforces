#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int ot, ct;
    cin >> ct >> ot;
    if (ot == 1 && ct == 0)
    {
        cout << "Yes" << endl;
        return 0;
    }
    if (ot == 0)
    {
        cout << "No" << endl;
        return 0;
    }
    int originial = ot, copied = ot - 1;
    if (copied <= 0)
    {
        cout << "No" << endl;
        return 0;
    }
    if (copied % 2 != ct % 2 || copied > ct)
    {
        cout << "No" << endl;
    }
    else
    {
        cout << "Yes" << endl;
    }
}