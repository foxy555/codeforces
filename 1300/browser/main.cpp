#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, pos, l, r;
    cin >> n >> pos >> l >> r;

    if (l == 1 && r == n)
    {
        cout << 0 << endl;
        return 0;
    }
    if (l == 1)
    {
        cout << abs(pos - r) + 1 << endl;
        return 0;
    }
    if (r == n)
    {
        cout << abs(pos - l) + 1 << endl;
        return 0;
    }
    cout << min(abs(pos - l), abs(pos - r)) + 1 + r - l + 1 << endl;
}