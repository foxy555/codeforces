#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    vector<vector<int>> board;

    for (int i = 0; i < n; i++)
    {
        vector<int> line;
        for (int j = 0; j < m; j++)
        {
            int a;
            cin >> a;
            line.push_back(a);
        }
        board.push_back(line);
    }

    vector<int> ans(n);
    for (int i = 0; i < m; i++)
    {
        int finished = 0;
        for (int j = 0; j < n; j++)
        {
            int time = max(finished, ans[j]);
            ans[j] = time + board[j][i];
            finished = ans[j];
        }
    }
    for (int i = 0; i < n; i++)
    {
        cout << ans[i] << " ";
    }
}