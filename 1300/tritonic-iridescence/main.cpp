#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string s;
    cin >> s;

    for (int i = 0; i < n - 1; i++)
    {
        if (s[i] == s[i + 1] && s[i] != '?')
        {
            cout << "NO" << endl;
            return 0;
        }
    }
    if (s[0] == '?' || s[n - 1] == '?')
    {
        cout << "YES" << endl;
        return 0;
    }
    int j = 0;
    for (int i = 0; i < n; i++)
    {
        if (s[i] == '?')
        {
            j = i + 1;
            while (s[j] == '?')
                j++;
            if (s[j] == s[i - 1])
            {
                cout << "YES" << endl;
                return 0;
            }
            else
            {
                if (j - i > 1)
                {
                    cout << "YES" << endl;
                    return 0;
                }
            }
        }
    }
    cout << "NO" << endl;
}