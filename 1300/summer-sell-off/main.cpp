#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, f;
    cin >> n >> f;
    vector<pair<ll, ll>> nlist;
    vector<pair<ll, pair<ll, ll>>> sellout;
    ll smaller = 0;
    for (ll i = 0; i < n; i++)
    {
        ll a, b;
        cin >> a >> b;
        if (a == 0 || b == 0)
        {
            continue;
        }
        if (a < b)
        {
            smaller++;
            sellout.push_back(make_pair(b - a, make_pair(a, b)));
        }
        else
        {
            nlist.push_back(make_pair(a, b));
        }
    }
    sort(sellout.rbegin(), sellout.rend());
    sort(nlist.rbegin(), nlist.rend());
    ll total = 0;
    vector<pair<ll, int>> realmoney;
    //vector<bool> forbidden(sellout.size(), 0);

    for (ll i = 0; i < sellout.size(); i++)
    {
        realmoney.push_back(make_pair(min(sellout[i].second.first * 2, sellout[i].second.second) - min(sellout[i].second.first, sellout[i].second.second), i));
    }
    sort(realmoney.rbegin(), realmoney.rend());
    for (ll i = 0; i < realmoney.size(); i++)
    {
        if (f != 0)
        {
            total += min(sellout[realmoney[i].second].second.first * 2, sellout[realmoney[i].second].second.second);
            //cout << min(sellout[realmoney[i].second].second.first * 2, sellout[realmoney[i].second].second.second) << endl;
            //forbidden[realmoney[i].second] = 1;
            f--;
        }
        else
        {
            total += min(sellout[realmoney[i].second].second.first, sellout[realmoney[i].second].second.second);
            //cout << min(sellout[realmoney[i].second].second.first, sellout[realmoney[i].second].second.second) << endl;
        }
    }
    //for (ll i = 0; i < sellout.size(); i++)
    //{
    //    if (forbidden[i] == 0)
    //    {
    //        total += min(sellout[i].second.first, sellout[i].second.second);
    //    }
    //}
    for (ll i = 0; i < nlist.size(); i++)
    {
        total += min(nlist[i].first, nlist[i].second);
    }
    cout << total << endl;
}