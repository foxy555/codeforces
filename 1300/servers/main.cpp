#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, q;
    cin >> n >> q;

    vector<int> servers(n);
    int lastused = 0;
    while (q--)
    {
        int a, b, c;
        cin >> a >> b >> c;
        vector<int> free;
        for (int i = 0; i < n; i++)
        {
            servers[i] -= a - lastused;
            if (servers[i] <= 0)
            {
                free.push_back(i);
            }
        }
        if (free.size() >= b)
        {
            int ans = 0;
            for (int i = 0; i < b; i++)
            {
                servers[free[i]] = c;
                ans += free[i] + 1;
            }
            cout << ans << endl;
        }
        else
        {
            cout << -1 << endl;
        }
        lastused = a;
    }
}