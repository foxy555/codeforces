#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(n);

        cin >> nlist[0];
        if (n == 1)
        {
            cout << "YES" << endl;
            continue;
        }
        for (int i = 1; i < n; i++)
        {
            cin >> nlist[i];
        }

        int ascloc = -1;
        for (int i = 0; i < n; i++)
        {
            if (nlist[i] < i)
            {
                break;
            }
            ascloc = i;
        }
        int dloc = -1;
        for (int i = n - 1; i >= 0; i--)
        {
            if (nlist[i] < n - 1 - i)
                break;
            dloc = i;
        }
        if (dloc <= ascloc)
        {
            cout << "YES\n";
        }
        else
        {
            cout << "NO\n";
        }
    }
}