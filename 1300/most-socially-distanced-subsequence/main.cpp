#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(n);
        vector<int> loc(n + 5);
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
            loc[nlist[i] - 1] = i;
        }
        bool asc = 0;
        vector<int> answer;
        answer.push_back(nlist[0]);
        if (nlist[0] < nlist[1])
            asc = 1;
        for (int i = 0; i < n; i++)
        {
            if (asc == 1 && nlist[i] > nlist[i + 1] || i == n - 1)
            {
                asc = 0;
                //if (i == n - 1 && nlist[i] < nlist[i + 1])
                //    i++;
                answer.push_back(nlist[i]);
                continue;
            }

            if (asc == 0 && nlist[i] < nlist[i + 1] || i == n - 1)
            {
                asc = 1;
                //if (i == n - 1 && nlist[i] > nlist[i + 1])
                //    i++;
                answer.push_back(nlist[i]);
            }
        }
        cout << answer.size() << endl;
        for (int i = 0; i < answer.size(); i++)
        {
            cout << answer[i] << " ";
        }
        cout << endl;
    }
}