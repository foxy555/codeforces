#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    int q;
    cin >> q;
    while (q--)
    {
        int l, r, k;
        cin >> l >> r >> k;
        int remain = k % (r - l + 1);
        if (remain > 0)
        {
            string sub = s.substr(l - 1, r - l + 1);
            string re = sub.substr((sub.length() - remain), remain);
            sub.erase(sub.begin() + (sub.length() - remain), sub.end());
            string replacement = re + sub;
            int c = 0;
            for (int i = l - 1; i < r; i++)
            {
                s[i] = replacement[c];
                c++;
            }
        }
    }
    cout << s << endl;
}