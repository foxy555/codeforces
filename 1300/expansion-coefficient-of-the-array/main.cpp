#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
        if (nlist[i] == 0)
        {
            cout << 0 << endl;
            return 0;
        }
    }
    int answer = INT_MAX;
    for (int i = 0; i < n; i++)
    {
        answer = min(answer, nlist[i] / max(i, n - 1 - i));
    }
    cout << answer << endl;
}