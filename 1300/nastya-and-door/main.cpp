#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, k;
        cin >> n >> k;
        vector<int> nlist(n);
        vector<int> peaksum(n, 0);
        int ps = 0;
        cin >> nlist[0] >> nlist[1];
        for (int i = 2; i < n; i++)
        {
            cin >> nlist[i];
            if (nlist[i - 2] < nlist[i - 1] && nlist[i - 1] > nlist[i])
            {
                ps++;
            }
            peaksum[i - 1] = ps;
        }
        peaksum[n - 1] = ps;
        int ma = INT_MIN;
        vector<pair<int, int>> peaks;
        int loc = -1;
        for (int i = 0; i < n - k + 1; i++)
        {
            int temp = peaksum[i + k - 2] - peaksum[i];
            if (temp > ma)
            {
                ma = temp;
                loc = i;
            }
            //for (int j = i + 1; j < i + k - 1; j++)
            //{
            //    if (nlist[j - 1]<nlist[j] && nlist[j]> nlist[j + 1])
            //    {
            //        temp++;
            //
            //    }
            //}
            //pair<int,int> current(i,temp);
            //peaks.push_back(current);
            //ma = max(ma, temp);
        }
        cout << ma + 1 << " " << loc + 1 << endl;
    }
}
