#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    map<int, int> width;
    set<int> w;
    for (int i = 0; i < n; i++)
    {
        int a;
        cin >> a;
        width[a] = i;
        w.insert(a);
    }
    string s;
    cin >> s;

    set<int> oneleft;
    oneleft.insert(*w.begin());
    cout << width[*w.begin()] + 1 << " ";
    w.erase(*w.begin());
    for (int i = 1; i < 2 * n; i++)
    {
        if (s[i] == '1')
        {
            int wid = *oneleft.rbegin();
            cout << width[wid] + 1 << " ";
            oneleft.erase(wid);
        }
        else
        {
            int wid = *w.begin();
            w.erase(wid);
            oneleft.insert(wid);
            cout << width[wid] + 1 << " ";
        }
    }
    cout << endl;
}