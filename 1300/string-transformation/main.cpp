#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    if (s.length() < 26)
    {
        cout << -1 << endl;
        return 0;
    }

    int a = 97;
    int sum = 0;
    for (int i = 0; i < s.length(); i++)
    {
        if (s[i] <= a)
        {
            s[i] = a;
            a++;
            sum++;
        }
        if (sum == 26)
            break;
    }
    if (sum >= 26)
        cout << s << endl;
    else
    {
        cout << -1 << endl;
    }
}