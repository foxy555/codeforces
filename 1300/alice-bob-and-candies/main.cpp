#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(n);
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        int alice = 0, bob = n - 1;
        int a = 0, b = 0, c = 0, s = 0;
        while (true)
        {
            bool end = false;
            if (nlist[alice] == -1)
                break;
            int temp = nlist[alice];
            a += temp;
            nlist[alice] = -1;
            alice++;
            while (temp <= c)
            {
                if (nlist[alice] == -1)
                {
                    end = 1;
                    break;
                }
                temp += nlist[alice];
                a += nlist[alice];
                nlist[alice] = -1;
                alice++;
                if (alice >= n)
                {
                    end = 1;
                    break;
                }
            }
            s++;
            c = temp;
            if (end)
                break;
            end = 0;
            if (nlist[bob] == -1)
                break;
            temp = nlist[bob];
            b += temp;
            nlist[bob] = -1;
            bob--;
            while (temp <= c)
            {
                if (nlist[bob] == -1)
                {
                    end = 1;
                    break;
                }
                temp += nlist[bob];
                b += nlist[bob];
                nlist[bob] = -1;
                bob--;
                if (bob < 0)
                {
                    end = 1;
                    break;
                }
            }
            s++;
            c = temp;
            if (end)
                break;
            end = 0;
        }
        cout << s << " " << a << " " << b << endl;
    }
}