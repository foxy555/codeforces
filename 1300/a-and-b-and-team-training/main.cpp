#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    int ans = 0;
    while (n >= 0 && m >= 0)
    {
        if (m > n)
        {
            m -= 2;
            n--;
        }
        else
        {
            n -= 2;
            m--;
        }
        if (m >= 0 && n >= 0)
            ans++;
    }
    cout << ans << endl;
}