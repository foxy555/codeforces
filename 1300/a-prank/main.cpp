#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n + 1);
    for (int i = 1; i <= n; i++)
    {
        cin >> nlist[i];
    }
    nlist.push_back(1001);
    int ans = 0;
    int counter = 1;
    for (int i = 1; i <= n + 1; i++)
    {
        if (nlist[i] == nlist[i - 1] + 1)
        {
            counter++;
        }
        else
        {
            ans = max(ans, counter);
            counter = 1;
        }
    }
    ans = max(ans, counter);
    if (nlist[0] == 1 && nlist.back() == n)
        ans = n - 1;
    cout << max(0, ans - 2) << endl;
}