#include <bits/stdc++.h>

#define ll long long

using namespace std;

set<ll> findfactors(ll x)
{
    set<ll> result;
    ll i = 1;
    while (i * i <= x)
    {
        if (x % i == 0)
        {
            result.insert(i);
            if (x / i != i)
            {
                result.insert(x / i);
            }
        }
        i++;
    }
    return result;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> nlist(n);
    set<ll> nset;
    for (ll i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    sort(nlist.begin(), nlist.end());
    ll ma = nlist.back();
    nset.erase(ma);
    ll num = 0;
    for (ll i = 0; i < n; i++)
    {
        num += ma - nlist[i];
        nset.insert(num);
    }
    nset.erase(0);
    set<ll> factors = findfactors(num);
    //factors.erase(1);
    while (!factors.empty())
    {
        ll a = *factors.rbegin();
        factors.erase(a);
        set<ll>::iterator it = nset.begin();
        bool ok = true;
        while (it != nset.end())
        {
            ll t = *it;
            if (t % a != 0)
            {
                ok = false;
                break;
            }
            it++;
        }
        if (ok)
        {
            cout << num / a << " ";
            cout << a << endl;
            return 0;
        }
    }
}