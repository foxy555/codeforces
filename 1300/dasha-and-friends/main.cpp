#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, l;
    cin >> n >> l;
    vector<int> alist(n), blist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> alist[i];
    }
    for (int i = 0; i < n; i++)
    {
        cin >> blist[i];
    }
    sort(alist.begin(), alist.end());
    sort(blist.begin(), blist.end());
    if (blist == alist)
    {
        cout << "YES" << endl;
        return 0;
    }
    for (int i = 0; i < l * 2; i++)
    {
        for (int j = 0; j < n; j++)
        {
            alist[j]++;
            if (alist[j] >= l)
                alist[j] -= l;
        }
        sort(alist.begin(), alist.end());
        if (alist == blist)
        {
            cout << "YES" << endl;
            return 0;
        }
    }
    cout << "NO" << endl;
}