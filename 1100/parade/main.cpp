#include <bits/stdc++.h>

using namespace std;

int main()
{
    int row;
    cin >> row;
    vector<int> left(row);
    vector<int> right(row);
    for(int i = 0; i < row; i++)
    {
        cin >> left[i] >> right[i];
    }
    int tleft= accumulate(left.begin(), left.end(), 0);
    int tright = accumulate(right.begin(),right.end(), 0);
    int maxbeauty = abs(tleft - tright);
    int maxlocation = 0;
    bool ok = false;
    for(int i = 0; i < row; i++)
    {
        if(abs((tleft - left[i] + right[i]) - (tright - right[i] + left[i])) > maxbeauty)
        {
            ok = true;
            maxbeauty = abs((tleft - left[i] + right[i]) - (tright - right[i] + left[i]));
            maxlocation = i;
        }
    }
    if(ok)
        cout << maxlocation + 1 << endl;
    else
        cout << 0 << endl;
}
