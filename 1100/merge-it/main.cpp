#include <bits/stdc++.h>

using namespace std;
int findrequest(vector<int> numlist, int l)
{
    for(int i = numlist.size() - 1; i >= 0; i--)
    {
        if((numlist[l] + numlist[i]) % 3 == 0)
        {
            return i;
        }
    }
    return -1;
}
int main()
{
    int tc;
    cin >> tc;
    while(tc--)
    {
        int tsize;
        cin >> tsize;
        vector<int> numlist;
        int divisible = 0;

        for(int i = 0; i < tsize; i++)
        {
            int a;
            cin >> a;
            if(a % 3 == 0)
                divisible++;
            else
                numlist.push_back(a % 3);
        }
        sort(numlist.rbegin(), numlist.rend());
        vector<int> unsuccessful;
        for(int i = 0; i < numlist.size(); i++)
        {
            int location = findrequest(numlist, i);
            if(location != -1)
            {
                numlist[i] += numlist[location];
                numlist.erase(numlist.begin() + location);
            }
            else
                unsuccessful.push_back(numlist[i]);

        }
        if(accumulate(unsuccessful.begin(), unsuccessful.end(), 0) > 2)
        {
            sort(unsuccessful.rbegin(), unsuccessful.rend());
            for(int i = 1; i < unsuccessful.size(); i++)
            {
                unsuccessful[i] += unsuccessful[i - 1];
                //unsuccessful.erase(unsuccessful.begin() + i - 1);
                if(unsuccessful[i] % 3 == 0)
                {
                    divisible++;
                }
            }
        }
        for(int i = 0; i < numlist.size(); i++)
        {
            if(numlist[i] % 3 == 0)
                divisible++;
        }
        cout << divisible << endl;

        
    }
}
