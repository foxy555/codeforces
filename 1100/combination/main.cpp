#include <bits/stdc++.h>

using namespace std;

int main()
{
    int cardnum;
    cin >> cardnum;
    vector<int> cpoints(cardnum);
    vector<int> cmoves(cardnum);
    
    int maxmoves = 1;
    int maxpoints = 0;
    for(int i = 0; i < cardnum; i++)
    {
        cin >> cpoints[i] >> cmoves[i];
        maxmoves += cmoves[i] - 1;
        maxpoints += cpoints[i];
    }

    if(maxmoves >= cardnum)
    {
        cout << maxpoints << endl;
        return 0;
    }

    int points = 0;

    int totalmoves = 1;
    for(int i = 0; i < cardnum; i++)
    {
        if(cmoves[i] != 0)
        {
            points += cpoints[i];
            cpoints[i] = 0;
            totalmoves--;
            totalmoves += cmoves[i];
            cmoves[i] = 0;
        }
    }
    sort(cpoints.rbegin(), cpoints.rend());
    for(int i = 0; i < cardnum; i++)
    {
        if(totalmoves != 0)
        {
            points += cpoints[i];
            totalmoves--;
        }
    }
    cout << points << endl;
}
