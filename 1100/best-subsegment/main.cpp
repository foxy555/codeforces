#include <bits/stdc++.h>
 
using namespace std;

int main()
{
    int l;
    cin >> l;
    vector<int> ar;
    for(int i = 0; i < l; i++)
    {
        int num;
        cin >> num;
        ar.push_back(num);
    }
    vector<int> oldar = ar;

    sort(ar.rbegin(), ar.rend());

    int high = ar[0];
    
    int longest = 0;
    int counter = 0;
    for(int i = 0; i < l; i++)
    {
        if(oldar[i] == high)
        {
            counter++;
        }
        else
        {
            if(counter > longest)
                longest = counter;
            counter = 0;
        }
    }
    cout << max(longest,counter) << endl;
}
