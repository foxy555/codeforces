#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    
    while(tc--)
    {
        vector<int> plays;
        vector<int> clears;

        int n;
        cin >> n;

        bool ok = true;
        for(int i = 0; i < n; i++)
        {
            int p,c;
            cin >> p >> c;

            plays.push_back(p);
            clears.push_back(c);
            
            if(c > p)
                ok = false;
            if(c == p && p != 0)
            {
                if(i > 0)
                {
                    if(plays[i - 1] == p && clears[i - 1] != c)
                        ok = false;
                }
            }
            if(i > 0)
                if(p - plays[i - 1] < c - clears[i - 1])
                ok = false;

        }
        vector<int> sortedlist = plays;

        sort(sortedlist.begin(), sortedlist.end());
        
        for(int i = 0; i < n; i++)
        {
            if(plays[i] != sortedlist[i])
            {
                ok = false;
                break;
            }
        }
        sortedlist = clears;

        sort(sortedlist.begin(), sortedlist.end());

        for(int i = 0; i < n; i++)
        {
            if(clears[i] != sortedlist[i])
            {
                ok = false;
                break;
            }
        }
        if(ok)
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
}
