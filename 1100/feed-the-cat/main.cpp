#include <bits/stdc++.h>

using namespace std;

int main()
{
    int hour, minute, hunger, hrate, price, frate;
    cin >> hour >> minute >> hunger >> hrate >> price >> frate;

    vector<double> pricear;
    int mealnum = 0;
    while(mealnum * frate < hunger) mealnum++;

    pricear.push_back(mealnum * price);
    
    int m = hour * 60 + minute;
    if(m >= 1200)
    {
        mealnum = 0; 
        while(mealnum * frate < hunger) mealnum++;
        pricear.push_back(mealnum * price * 0.8);
    }
    else
    {
        hunger += hrate * (1200 - m);
        mealnum = 0;
        while(mealnum * frate < hunger) mealnum++;
        pricear.push_back(mealnum * price * 0.8);
    }
    sort(pricear.begin(),pricear.end());

    cout << pricear[0] << endl;
}
