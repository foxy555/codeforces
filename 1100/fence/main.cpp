#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int len, interval;
    cin >> len >> interval;

    vector<int> height(150005, 0);
    for(int i = 1; i <= len; i++)
        cin >> height[i];
    for(int i = 1; i <= len; i++)
        height[i] += height[i - 1];
    int minsum = INT_MAX;
    int location = 0;
    int sum = 0;
    for(int i = 1; i <= len - interval + 1; i++)
    {
        sum = height[i + interval - 1] - height[i - 1];
        //sum = accumulate(height.begin() + i, height.begin() + i + interval, 0);
        if(sum < minsum)
        {
            minsum = sum;
            location = i;
        }
    }
    cout << location << endl;
}
