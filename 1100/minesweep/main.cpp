#include <bits/stdc++.h>

using namespace std;

int getbombs(const vector<string> &board, int x, int y)
{
    int bombsct = 0;
    for(int i = -1; i < 2; ++i)
    {
        if(board[x - 1][y + i] == '*')
            bombsct++;
        if(board[x + 1][y + i] == '*')
            bombsct++;
    }
    if(board[x][y - 1] == '*')
        bombsct++;
    if(board[x][y + 1] == '*')
        bombsct++;
    return bombsct;
    //if((int)board[x][y] <= 56 && (int)board[x][y] >=49)
     //   return (int)board[x][y] - 48;
}
int main()
{
    int lines, len;
    cin >> lines >> len;

    vector<string> board(lines + 2);
    string emptystring(len + 2, '.');
    board[0] = emptystring;
    board[lines + 1] = emptystring;
    for(int i = 1; i < lines + 1; i++)
    {
        string line;
        cin >> line;
        board[i] = "." + line + ".";
    }
    for(int x = 1; x < lines + 1; x++)
    {
        for(int y = 1; y < len + 1; y++)
        {
            if((int)board[x][y] <= 56 && (int)board[x][y] >=49)
            {
                int boardbomb = (int)board[x][y] - 48;
                if(getbombs(board, x, y) != boardbomb)
                {
                    cout << "NO" << endl;
                    return 0;
                }
            }
            if(board[x][y] == '.')
            {
                if(getbombs(board,x,y))
                {
                    cout << "NO" << endl;
                    return 0;
                }
            }
        }
    }
    cout << "YES" << endl;
}
