#include <bits/stdc++.h>

using namespace std;

int main()
{
    cin.tie(NULL);
    ios_base::sync_with_stdio(0);
    int tc;
    cin >> tc;

    while(tc--)
    {
        int tsize;
        cin >> tsize;
        
        vector<int> students;
        for(int i = 0; i < tsize; i++)
        {
            int a;
            cin >> a;
            students.push_back(a);
        }
        if(tsize == 1)
        {
            cout << 0 << endl;
            continue;
        }
        sort(students.begin(), students.end());

        int maxsame = 1;
        int previous = students[0];
        int same = 0;
        for(int i = 0; i < tsize; i++)
        {
            if(students[i] == previous && i != tsize - 1)
            {
                same++;
            }
            else
            {
                if(i == tsize - 1 && students[i] == previous)
                    same++;
                if(same > maxsame)
                {
                    maxsame = same;
                }
                previous = students[i];
                same = 1;
            }
        }
        
        int uniquen = unique(students.begin(), students.end()) - students.begin();
        //cout << max(uniquen - 1, min(uniquen, maxsame - 1)) << endl;
        if(maxsame > uniquen)
            cout << uniquen << endl;
        else if(maxsame == uniquen)
            cout << uniquen - 1 << endl;
        else
            cout << maxsame << endl;

    }
}
