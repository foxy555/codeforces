#include <bits/stdc++.h>

using namespace std;

int main()
{
    int passn, planen;
    cin >> passn >> planen;
    vector<int> tickets;
    for(int i = 0; i < planen; i++)
    {
        int a;
        cin >> a;
        tickets.push_back(a);
    }
    vector<int> maxtickets = tickets;
    vector<int> mintickets = tickets;
    int minprice = 0;
    int maxprice = 0;
    while(passn != 0)
    {
        sort(maxtickets.rbegin(), maxtickets.rend());
        maxprice+=maxtickets[0];
        maxtickets[0]--;
        passn--;
        sort(mintickets.begin(), mintickets.end());
        for(int i = 0; i < mintickets.size(); i++)
        {
            if(mintickets[i] != 0)
            {
                minprice+=mintickets[i];
                mintickets[i]--;
                break;
            }
        }
    }
    cout << maxprice << " " << minprice << endl;
}
