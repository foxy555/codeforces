#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, l, r, s;
        cin >> n >> l >> r >> s;
        int ma = 0, mi = 0;
        int interval = r - l + 1;
        for (int i = 0; i < interval; ++i) {
            mi += (i + 1);
            ma += (n - i);
        }
        if (s < mi || s > ma) {
            cout << -1 << endl;
            continue;
        }

        vector<int> occupied;
        for (int i = 0; i < interval; ++i) {
            occupied.push_back(i + 1);
        }
        int sumbefore = 0, sumafter = 0;
        for (int i = 0; i < interval; ++i) {
            sumbefore += occupied[i];
        }
        int maxallowed = n;
        for (int i = interval - 1; i >= 0; --i) {
            sumbefore -= occupied[i];
            int left = s - (sumbefore + occupied[i] + sumafter);
            if (left + occupied[i] <= maxallowed) {
                occupied[i] = left + occupied[i];
                break;
            } else {
                occupied[i] = maxallowed;
                --maxallowed;
                sumafter += occupied[i];
            }
        }
        set<int> occupiedset(occupied.begin(), occupied.end());
        vector<int> ans(n, 0);
        vector<int> avail;
        avail.push_back(-1);
        for (int i = 1; i <= n; ++i) {
            if (!occupiedset.count(i)) {
                avail.push_back(i);
            }
        }
        int j = 0;
        for (int i = l - 1; i < r; ++i) {
            ans[i] = occupied[j++];
        }
        for (int i = 0; i < l - 1; ++i) {
            ans[i] = avail.back();
            avail.pop_back();
        }
        for (int i = r; i < n; ++i) {
            ans[i] = avail.back();
            avail.pop_back();
        }
        for (int i = 0; i < n; ++i) {
            cout << ans[i] << " ";
        }
        cout << endl;
    }
}
