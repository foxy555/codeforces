#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;

    while (tc--)
    {
        ll n;
        cin >> n;
        vector<ll> nlist(n + 2);
        map<ll, ll> lookup;
        for (ll i = 0; i < n + 2; ++i)
        {
            cin >> nlist[i];
            ++lookup[nlist[i]];
        }
        sort(nlist.begin(), nlist.end());

        ll sum1 = 0, sum2 = 0;
        for (ll i = 0; i < n; ++i)
        {
            sum1 += nlist[i];
        }
        for (ll i = 0; i < n + 1; ++i)
        {
            sum2 += nlist[i];
        }
        if (sum1 == nlist[n])
        {
            for (ll i = 0; i < n; ++i)
            {
                cout << nlist[i] << " ";
            }
            cout << endl;
            continue;
        }
        ll leftover = sum2 - nlist.back();
        if (leftover <= 0)
        {
            cout << -1 << endl;
            continue;
        }
        if (lookup[leftover] - (leftover == nlist.back()) <= 0)
        {
            cout << -1 << endl;
            continue;
        }

        ll remove = leftover;
        for (ll i = 0; i < n + 1; ++i)
        {
            if (nlist[i] == remove)
            {
                remove = 0;
            }
            else
            {
                cout << nlist[i] << " ";
            }
        }
        cout << endl;
    }
}
