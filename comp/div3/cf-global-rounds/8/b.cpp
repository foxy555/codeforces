#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll k;
    cin >> k;
    cout << "codeforces";
    string s = "codeforces";
    if (k > 1)
    {
        if (k <= 5)
        {
            string sub(k - 1, 's');
            cout << sub << endl;
            return 0;
        }
        ll ans;
        for (ll i = 0; i < k; i++)
        {
            if (i * (i - 1) >= k)
            {
                ans = i;
                break;
            }
        }
        cout << ans << endl;
    }
    else
    {
        cout << '\n';
    }
}