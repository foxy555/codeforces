#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        ll n;
        cin >> n;

        ll d = 1;
        ll dcnt = 1;
        ll ans = 0;
        ll increment = 1;
        while (true) {
            string temp = "";
            for (int i = 0; i < dcnt; ++i) {
                temp += d + '0';
            }
            if (stol(temp) <= n) {
                ++ans;
            } else
                break;
            ++d;
            if (d == 10) {
                d = 1;
                ++dcnt;
            }
        }
        cout << ans << endl;
    }
}
