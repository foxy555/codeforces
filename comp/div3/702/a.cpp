#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        int cnt = 0;
        for (int i = 0; i < n - 1; ++i)
        {
            while (min(nlist[i], nlist[i + 1]) * 2 <
                   max(nlist[i], nlist[i + 1]))
            {
                if (nlist[i] < nlist[i + 1])
                {
                    nlist[i] *= 2;
                    ++cnt;
                }
                else
                {
                    nlist[i] = nlist[i] / 2 + nlist[i] % 2;
                    ++cnt;
                }
            }
        }
        cout << cnt << endl;
    }
}
