#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;
    while (tc--)
    {
        ll n;
        cin >> n;

        vector<pair<ll, ll>> nlist(n);
        for (ll i = 0; i < n; ++i)
        {
            cin >> nlist[i].first;
            nlist[i].second = i;
        }
        sort(nlist.begin(), nlist.end());
        vector<pair<ll, ll>> clist;
        clist.push_back({nlist.front().first, 0});
        for (ll i = 0; i < n; ++i)
        {
            if (nlist[i].first == clist.back().first)
                ++clist.back().second;
            else
                clist.push_back({nlist[i].first, 1});
        }
        ll min_to_win = -1;
        vector<ll> sum(clist.size());
        sum[0] = clist[0].first * clist[0].second;
        for (ll i = 0; i < clist.size(); ++i)
        {
            ll cnt = 0;
            if (i > 0)
            {
                cnt = sum[i - 1];
            }
            cnt += clist[i].first * clist[i].second;
            sum[i] = cnt;
            bool ok = 1;
            for (ll j = i + 1; j < clist.size(); ++j)
            {
                if (cnt >= clist[j].first)
                {
                    cnt += clist[j].first * clist[j].second;
                }
                else
                {
                    ok = 0;
                    break;
                }
            }
            if (ok)
            {
                min_to_win = clist[i].first;
                break;
            }
        }
        vector<ll> ans;
        for (ll i = 0; i < n; ++i)
        {
            if (nlist[i].first >= min_to_win)
            {
                ans.push_back(nlist[i].second + 1);
            }
        }
        sort(ans.begin(), ans.end());
        cout << ans.size() << endl;
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
