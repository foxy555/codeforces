#include "bits/stdc++.h"

#define ll long long

using namespace std;
vector<int> ans(100);
vector<int> preans(101);
vector<int> nlist(100);
vector<int> pos(100 + 1);
int n;
void dfs(int prev_n, int depth)
{
    int left = -1, right = -1;
    int index = pos[prev_n];
    int big = -1;
    for (int i = index - 1; i >= 0; --i)
    {
        if (nlist[i] > prev_n)
            break;
        big = max(big, nlist[i]);
    }
    left = big;

    big = -1;
    for (int i = index + 1; i < n; ++i)
    {
        if (nlist[i] > prev_n)
            break;
        big = max(big, nlist[i]);
    }
    right = big;
    if (left != -1)
    {
        preans[left] = depth;
        dfs(left, depth + 1);
    }
    if (right != -1)
    {
        preans[right] = depth;
        dfs(right, depth + 1);
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        cin >> n;

        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
            pos[nlist[i]] = i;
        }
        preans[n] = 0;
        dfs(n, 1);
        for (int i = 1; i < n + 1; ++i)
        {
            ans[pos[i]] = preans[i];
        }
        for (int i = 0; i < n; ++i)
        {
            cout << ans[i] << " ";
        }
        cout << endl;
    }
}
