#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n);
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
        }

        set<int> num;
        for (int i = 0; i < n; ++i) {
            num.insert(i + 1);
        }

        vector<pair<int, int>> compress;
        compress.push_back({nlist.front(), 1});
        for (int i = 1; i < n; ++i) {
            if (compress.back().first == nlist[i])
                ++compress.back().second;
            else
                compress.push_back({nlist[i], 1});
        }
        vector<int> ans(n, 0);
        int index = 0;
        for (int i = 0; i < compress.size(); ++i) {
            ans[index++] = compress[i].first;
            num.erase(compress[i].first);
            for (int j = 1; j < compress[i].second; ++j) {
                ans[index++] = *num.begin();
                num.erase(num.begin());
            }
        }
        for (int i = 0; i < n; ++i) {
            cout << ans[i] << " ";
        }
        cout << endl;
        num.clear();
        for (int i = 0; i < n; ++i) {
            num.insert(i + 1);
        }
        index = 0;
        for (int i = 0; i < compress.size(); ++i) {
            ans[index++] = compress[i].first;
            num.erase(compress[i].first);
            for (int j = 1; j < compress[i].second; ++j) {
                auto it = num.lower_bound(compress[i].first);
                --it;
                ans[index++] = *it;
                num.erase(it);
            }
        }
        for (int i = 0; i < n; ++i) {
            cout << ans[i] << " ";
        }
        cout << endl;
    }
}
