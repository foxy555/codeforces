#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;
    while (tc--) {
        ll n, m, x;
        cin >> n >> m >> x;

        ll px = x / n + 1;
        ll py = x % n;
        if (py == 0) {
            py = n;
            px--;
        }

        cout << (py - 1) * m + px << endl;
    }
}
