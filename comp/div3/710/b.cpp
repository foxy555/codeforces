#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, k;
        string s;
        cin >> n >> k >> s;

        vector<int> slist;
        for (int i = 0; i < n; ++i) {
            if (s[i] == '*') slist.push_back(i);
        }

        int ans = 1;
        int lastindex = 0;
        for (int i = 1; i < slist.size(); ++i) {
            if (slist[i] - slist[lastindex] > k) {
                ++ans;
                lastindex = i - 1;
                s[slist[lastindex]] = 'x';
            }
        }
        if (slist.size() != 1 && s[slist.back()] != 'x') ++ans;
        cout << ans << endl;
    }
}
