#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(n);
        int even = 0, odd = 0;
        for (int i = 0; i < n; i++)
        {
            int a;
            cin >> a;
            if (a % 2 == 0)
            {
                even++;
                nlist[i] = 0;
            }
            else
            {
                odd++;
                nlist[i] = 1;
            }
        }
        if (n % 2 == 0)
        {
            if (even != odd)
            {
                cout << -1 << endl;
                continue;
            }
        }
        else
        {
            if (even - 1 != odd)
            {
                cout << -1 << endl;
                continue;
            }
        }
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            if (nlist[i] % 2 != i % 2)
                ans++;
        }
        cout << ans / 2 << endl;
    }
}