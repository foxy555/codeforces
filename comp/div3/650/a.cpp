#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string b;
        cin >> b;
        string a = "";
        a = a + b[0];
        for (int i = 1; i < b.length() - 1; i++)
        {
            if (b[i] == b[i + 1])
            {
                a += b[i];
                i++;
            }
        }
        a += b[b.length() - 1];
        cout << a << endl;
    }
}