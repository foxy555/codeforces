#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        vector<pair<pair<int, int>, int>> nlist(n);
        vector<pair<pair<int, int>, int>> rlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i].first.first >> nlist[i].first.second;
            rlist[i].first.first = nlist[i].first.second;
            rlist[i].first.second = nlist[i].first.first;
            rlist[i].second = i;
            nlist[i].second = i;
        }

        sort(nlist.begin(), nlist.end());
        sort(rlist.begin(), rlist.end());
        vector<pair<pair<int, int>, int>> small;
        vector<pair<pair<int, int>, int>> rsmall;
        pair<int, int> minimum = {INT_MAX, INT_MAX};
        small.push_back(nlist.front());
        for (int i = 0; i < n; ++i)
        {
            minimum.first = min(minimum.first, nlist[i].first.first);
            minimum.second = min(minimum.second, nlist[i].first.second);
            if (nlist[i].first.first != small.back().first.first)
            {
                small.push_back(nlist[i]);
            }
        }
        rsmall.push_back(rlist.front());
        for (int i = 0; i < n; ++i)
        {
            if (rlist[i].first.first != rsmall.back().first.first)
            {
                rsmall.push_back(rlist[i]);
            }
        }
        vector<int> ans(n, -1);
        vector<pair<pair<int, int>, int>> additional;
        map<pair<int, int>, int> dp;
        for (int i = n - 1; i >= 0; --i)
        {
            if (dp[{nlist[i].first.first, nlist[i].first.second}] != 0)
            {
                ans[nlist[i].second] =
                    dp[{nlist[i].first.first, nlist[i].first.second}];
                continue;
            }
            if (nlist[i].first.first > small.front().first.first &&
                nlist[i].first.second > small.front().first.second)
            {
                ans[nlist[i].second] = small.front().second + 1;
                dp[{nlist[i].first.first, nlist[i].first.second}] =
                    ans[nlist[i].second];
                continue;
            }
            if (nlist[i].first.second > small.front().first.first &&
                nlist[i].first.first > small.front().first.second)
            {
                ans[nlist[i].second] = small.front().second + 1;
                dp[{nlist[i].first.first, nlist[i].first.second}] =
                    ans[nlist[i].second];
                continue;
            }
            if (nlist[i].first.first > rsmall.front().first.first &&
                nlist[i].first.second > rsmall.front().first.second)
            {
                ans[nlist[i].second] = rsmall.front().second + 1;
                dp[{nlist[i].first.first, nlist[i].first.second}] =
                    ans[nlist[i].second];
                continue;
            }
            if (nlist[i].first.second > rsmall.front().first.first &&
                nlist[i].first.first > rsmall.front().first.second)
            {
                ans[nlist[i].second] = rsmall.front().second + 1;
                dp[{nlist[i].first.first, nlist[i].first.second}] =
                    ans[nlist[i].second];
                continue;
            }
            for (int j = 0; j < additional.size(); ++j)
            {
                if (nlist[i].first.first > additional[j].first.first &&
                    nlist[i].first.second > additional[j].first.second)
                {
                    ans[nlist[i].second] = additional[j].second + 1;
                    dp[{nlist[i].first.first, nlist[i].first.second}] =
                        ans[nlist[i].second];
                    break;
                }
                if (nlist[i].first.second > additional[j].first.first &&
                    nlist[i].first.first > additional[j].first.second)
                {
                    ans[nlist[i].second] = additional[j].second + 1;
                    dp[{nlist[i].first.first, nlist[i].first.second}] =
                        ans[nlist[i].second];
                    break;
                }
            }
            if (ans[nlist[i].second] != -1)
                continue;
            cout << nlist[i].first.first << nlist[i].first.second << endl;
            for (int j = 0; j < n; ++j)
            {
                if (nlist[i].first.first > nlist[j].first.first &&
                    nlist[i].first.second > nlist[j].first.second)
                {
                    additional.push_back(nlist[j]);
                    ans[nlist[i].second] = nlist[j].second + 1;
                    break;
                }
                if (nlist[i].first.second > nlist[j].first.first &&
                    nlist[i].first.first > nlist[j].first.second)
                {
                    additional.push_back(nlist[j]);
                    ans[nlist[i].second] = nlist[j].second + 1;
                    break;
                }
                if (nlist[i].first.first > rlist[j].first.first &&
                    nlist[i].first.second > rlist[j].first.second)
                {
                    additional.push_back(rlist[j]);
                    ans[nlist[i].second] = rlist[j].second + 1;
                    break;
                }
                if (nlist[i].first.second > rlist[j].first.first &&
                    nlist[i].first.first > rlist[j].first.second)
                {
                    additional.push_back(rlist[j]);
                    ans[nlist[i].second] = rlist[j].second + 1;
                    break;
                }
            }
            dp[{nlist[i].first.first, nlist[i].first.second}] =
                ans[nlist[i].second];
        }
        for (int i = 0; i < n; ++i)
        {
            cout << ans[i] << " ";
        }
        cout << endl;
    }
}
