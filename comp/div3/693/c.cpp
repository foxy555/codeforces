#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;

    while (tc--)
    {
        ll n;
        cin >> n;
        vector<ll> nlist(n);
        for (ll i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        vector<ll> dp(n, 0);
        dp.back() = nlist.back();

        for (ll i = n - 2; i >= 0; --i)
        {
            dp[i] = nlist[i];
            if (i + nlist[i] < n)
            {
                dp[i] += dp[i + nlist[i]];
            }
        }
        ll high = 0;
        for (ll i = 0; i < n; ++i)
        {
            high = max(high, dp[i]);
        }
        cout << high << endl;
    }
}
