#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> charlist(26, 0);

        for (int i = 0; i < n; ++i)
        {
            string s;
            cin >> s;

            for (auto j : s)
            {
                charlist[j - 'a']++;
            }
        }
        bool ok = 1;
        for (int i = 0; i < 26; ++i)
        {
            if (charlist[i] % n != 0)
            {
                ok = 0;
            }
        }
        if (ok)
        {
            cout << "YES";
        }
        else
            cout << "NO";
        cout << endl;
    }
}
