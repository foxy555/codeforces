#include "bits/stdc++.h"

#define ll long long

using namespace std;
set<int> bad;
void factor(int n) {
    for (int i = 1; i <= sqrt(n); ++i) {
        if (n % i == 0) {
            if (n / i == i)
                bad.insert(i);
            else {
                bad.insert(i);
                bad.insert(n / i);
            }
        }
    }
}
int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int tc;
    cin >> tc;

    while (tc--) {
        bad.clear();
        int n;
        char c;
        string s;
        cin >> n >> c >> s;

        vector<int> nlist;
        for (int i = 0; i < n; ++i) {
            if (s[i] != c) {
                nlist.push_back(i + 1);
            }
        }
        if (nlist.size() == 0) {
            cout << 0 << endl;
            continue;
        }
        if (nlist.back() < n) {
            cout << 1 << endl << n << endl;
            continue;
        }
        // need to change the last
        // either find a number that works for all or use n and n - 1
        for (int i = 0; i < nlist.size(); ++i) {
            factor(nlist[i]);
        }
        bool ok = 0;
        int ans = 0;
        for (int i = 2; i < n; ++i) {
            if (bad.count(i) == 0) {
                ok = 1;
                ans = i;
            }
        }
        if (ok) {
            cout << 1 << endl << ans << endl;
        } else {
            cout << 2 << endl << n - 1 << " " << n << endl;
        }
    }
}
