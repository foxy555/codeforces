#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;
    while (tc--)
    {
        ll n, k;
        cin >> n >> k;
        vector<ll> nlist(n), old;
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        vector<ll> friends(k);
        for (int i = 0; i < k; i++)
        {
            cin >> friends[i];
        }
        sort(nlist.rbegin(), nlist.rend());
        old = nlist;
        sort(friends.begin(), friends.end());
        nlist.erase(unique(nlist.begin(), nlist.end()), nlist.end());
        vector<pair<ll, ll>> numbers;
        for (int i = 0; i < nlist.size(); i++)
        {
            numbers.push_back(make_pair(nlist[i], 0));
        }
        for (int i = 0; i < numbers.size(); i++)
        {
            numbers[i].second = count(old.begin(), old.end(), numbers[i].first);
        }
        ll ans = 0;
        for (int i = 0; i < k; i++)
        {
            ll ma = LONG_LONG_MIN, mi = LONG_LONG_MAX;
            for (int j = 0; j < numbers.size(); j++)
            {
                if (numbers[j].second > 0)
                {
                    numbers[j].second--;
                    friends[i]--;
                    ma = max(ma, numbers[j].first);
                    mi = min(mi, numbers[j].first);
                }
                if (friends[i] == 0)
                    break;
            }
            int l = 0;
            while (friends[i] != 0)
            {
                if (numbers[l].second != 0)
                {
                    if (friends[i] > numbers[l].second)
                    {
                        friends[i] -= numbers[l].second;
                        numbers[l].second = 0;
                        ma = max(ma, numbers[l].first);
                        mi = min(mi, numbers[l].first);
                        continue;
                    }
                    numbers[l].second -= friends[i];
                    friends[i] = 0;
                    ma = max(ma, numbers[l].first);
                    mi = min(mi, numbers[l].first);
                }
                l++;
            }
            ans += ma + mi;
        }
        cout << ans << endl;
    }
}