#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        int head = 0;
        if (*nlist.begin() == 1)
            for (int i = 0; i < n; i++)
            {
                if (nlist[i] == i + 1)
                {
                    head = i + 1;
                }
                else
                    break;
            }
        if (head == n)
        {
            cout << 0 << endl;
            continue;
        }
        int tail = n;
        if (nlist.back() == n)
        {
            for (int i = n - 1; i >= 0; i--)
            {
                if (nlist[i] == i + 1)
                {
                    tail = i;
                }
                else
                    break;
            }
        }
        int ans = 1;
        for (int i = head; i < tail; i++)
        {
            if (nlist[i] == i + 1)
            {
                ans = 2;
                break;
            }
        }
        cout << ans << endl;
    }
}
