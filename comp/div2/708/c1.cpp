#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, k;
        cin >> n >> k;
        if (n % 2) {
            cout << 1 << " " << n / 2 << " " << n / 2 << endl;
        } else {
            int half = n / 2;
            if (half % 2) {
                cout << 2 << " " << half - 1 << " " << half - 1 << endl;
            } else {
                cout << half << " " << half / 2 << " " << half / 2 << endl;
            }
        }
    }
}
