#include "bits/stdc++.h"

#define ll long long

using namespace std;
const int mxN = 1e3 + 5;
int parent[2][mxN];
int set_size[2][mxN];
void make_set() {
    for (int i = 0; i < mxN; ++i) {
        parent[0][i] = i;
        parent[1][i] = i;
        set_size[0][i] = 1;
        set_size[1][i] = 1;
    }
}
int find_set(int v, int p) {
    if (v == parent[p][v]) {
        return v;
    }
    return parent[p][v] = find_set(parent[p][v], p);
}

void union_sets(int a, int b, int p) {
    a = find_set(a, p);
    b = find_set(b, p);
    if (a != b) {
        if (set_size[p][a] < set_size[p][b]) {
            swap(a, b);
        }
        parent[p][b] = a;
        set_size[p][a] += set_size[p][b];
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int n, m1, m2;
    cin >> n >> m1 >> m2;
    bool al1[mxN][mxN];
    bool al2[mxN][mxN];
    memset(al1, 0, sizeof al1);
    memset(al2, 0, sizeof al2);
    make_set();
    for (int i = 0; i < m1; ++i) {
        int a, b;
        cin >> a >> b;
        union_sets(a, b, 0);
        al1[a][b] = 1;
        al1[b][a] = 1;
    }
    for (int i = 0; i < m2; ++i) {
        int a, b;
        cin >> a >> b;
        union_sets(a, b, 1);
        al2[a][b] = 1;
        al2[b][a] = 1;
    }
    vector<pair<int, int>> ans;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            if (i == j) {
                continue;
            }
            if (al1[i][j] || al1[j][i] || al2[i][j] || al2[i][j]) {
                continue;
            }
            if (find_set(i, 0) == find_set(j, 0) ||
                find_set(i, 1) == find_set(j, 1)) {
                continue;
            }
            ans.push_back({i, j});
            al1[i][j] = 1;
            al2[i][j] = 1;
            al1[j][i] = 1;
            al2[j][i] = 1;
            union_sets(i, j, 0);
            union_sets(i, j, 1);
        }
    }
    cout << ans.size() << endl;
    for (int i = 0; i < ans.size(); ++i) {
        cout << ans[i].first << " " << ans[i].second << endl;
    }
}
