#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int tc;
    cin >> tc;
    while (tc--) {
        int n;
        cin >> n;
        string s;
        cin >> s;

        bool prev = 0;
        char temp[2] = {'B', 'R'};
        int first = 0;
        for (int i = 0; i < n; ++i) {
            if (s[i] == '?')
                ++first;
            else {
                if (s[i] == 'B') prev = 0;
                if (s[i] == 'R') prev = 1;
                break;
            }
        }
        for (int i = first - 1; i >= 0; --i) {
            s[i] = temp[!prev];
            prev = !prev;
        }
        for (int i = 0; i < n; ++i) {
            if (s[i] == '?') {
                s[i] = temp[!prev];
            }
            if (s[i] == 'B') prev = 0;
            if (s[i] == 'R') prev = 1;
        }
        cout << s << endl;
        /*
        vector<pair<char, int>> compress;
        compress.push_back({s.front(), 0});
        for (int i = 0; i < n; ++i) {
            if (compress.back().first == s[i]) {
                ++compress.back().second;
            } else {
                compress.push_back({s[i], 1});
            }
        }
        vector<int> ilist(compress.size());
        for (int i = 0; i < compress.size(); ++i) {
            if (compress[i].first != '?') {
                ilist[i] = compress[i].second - 1;
            }
        }

        if (compress.size() == 1) {
            if (compress.front().first == '?') {
                char temp[2] = {'R', 'B'};
                for (int i = 0; i < n; ++i) {
                    cout << temp[i % 2];
                }
                cout << endl;
                continue;
            } else {
                cout << s << endl;
            }
        }
        */
    }
}
