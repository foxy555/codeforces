#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int tc;
    cin >> tc;
    while (tc--) {
        int n;
        cin >> n;
        vector<array<bool, 32>> nlist(n);
        for (int i = 0; i < n; ++i) {
            int a;
            cin >> a;
            int temp = 0;
            while (a) {
                if (a & 1) {
                    nlist[i][temp++] = 1;
                } else {
                    nlist[i][temp++] = 0;
                }
                a >>= 1;
            }
        }
        vector<bool> ans(32, 1);
        for (int i = 0; i < 32; ++i) {
            for (int j = 0; j < n; ++j) {
                if (nlist[j][i] == 0) {
                    ans[i] = 0;
                    break;
                }
            }
        }
        int a = 0;
        for (int i = 0; i < 32; ++i) {
            if (ans[i]) {
                a += pow(2, i);
            }
        }
        cout << a << endl;
    }
}
