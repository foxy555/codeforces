#include <bits/stdc++.h>

using namespace std;

int main(){
    int tests;
    cin >> tests;

    vector<int> testindex(tests);
    for(int i = 0; i < tests; i++)
        cin >> testindex[i];

    sort(testindex.begin(), testindex.end());
    int index = testindex[tests - 1] + 1;
    for(int i = 0; i < tests; i++)
    {
        if(testindex[i] != i + 1)
        {
            index = i + 1;
            break;
        }
    }

    cout << index << endl;
}
