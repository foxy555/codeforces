#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;
    vector<int> numlist(len);
    for (int i = 0; i < len; i++)
    {
        cin >> numlist[i];
    }
    int infoc = 0;
    int ans = 0;
    while (infoc != len)
    {
        for (int i = 0; i < len; i++)
        {
            if (infoc >= numlist[i] && numlist[i] != -1)
            {
                infoc++;
                numlist[i] = -1;
            }
        }
        if (infoc >= len)
            break;

        ans++;
        for (int i = len - 1; i >= 0; i--)
        {
            if (infoc >= numlist[i] && numlist[i] != -1)
            {
                infoc++;
                numlist[i] = -1;
            }
        }
        if (infoc >= len)
            break;

        ans++;
    }
    cout << ans << endl;
}