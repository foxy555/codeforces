#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    for(int t = 0; t < tc; t++)
    {
        int n, k;
        cin >> n >> k;
        
        int last = n - (k - 1) * 2;
        if(last % 2 == 0 && last > 0)
        {
            cout << "YES" << endl;
            for(int i = 0; i < k - 1; i++)
                cout << 2 << " ";
            cout << last << endl;
            continue;
        }
        last = n - (k - 1);
        if(last % 2 != 0 && last > 0)
        {
            cout << "YES" << endl;
            for(int i = 0; i < k - 1; i++)
                cout << 1 << " ";
            cout << last << endl;
            continue;
        }
        cout << "NO" << endl;
    }
}
