#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;
    len--;
    vector<int> votes;
    vector<int> aux(1005);
    int v;
    cin >> v;
    for (int i = 0; i < len; i++)
    {
        int a;
        cin >> a;
        votes.push_back(a);
        aux[a]++;
    }
    sort(votes.rbegin(), votes.rend());
    if (v > *votes.begin())
    {
        cout << 0 << endl;
        return 0;
    }
    int result = 0;
    while (v <= *votes.begin())
    {
        votes[0]--;
        result++;
        v++;
        sort(votes.rbegin(), votes.rend());
    }
    cout << result << endl;
}