#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;

    while (tc--)
    {
        int dkcount;
        cin >> dkcount;
        vector<int> princelist(dkcount, 1);

        int unhappydaughter = 0, emptykingdom = 0;
        for (int i = 0; i < dkcount; i++)
        {
            bool improveflag = true;
            int n;
            cin >> n;
            vector<int> wanted(n);
            for (int j = 0; j < n; j++)
                cin >> wanted[j];

            for (int j = 0; j < n; j++)
            {
                if (princelist[wanted[j] - 1] == 1)
                {
                    princelist[wanted[j] - 1] = 0;
                    improveflag = false;
                    break;
                }
            }
            if (improveflag && !unhappydaughter)
                unhappydaughter = i + 1;
        }
        if (unhappydaughter)
        {
            emptykingdom = distance(princelist.begin(), find(princelist.begin(), princelist.end(), 1)) + 1;
        }

        if (unhappydaughter)
        {
            cout << "IMPROVE" << endl
                 << unhappydaughter << " " << emptykingdom << endl;
        }
        else
        {
            cout << "OPTIMAL" << endl;
        }
    }
}