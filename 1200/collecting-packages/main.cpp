#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;

    while (tc--)
    {
        int len;
        cin >> len;
        vector<pair<int, int>> xy(len);

        for (int i = 0; i < len; i++)
        {
            cin >> xy[i].first >> xy[i].second;
        }
        sort(xy.begin(), xy.end());

        int px = xy[0].first;
        int py = xy[0].second;
        bool flag = true;
        for (int i = 1; i < len; i++)
        {
            if (xy[i].first != 0 || px != 0)
                if (xy[i].first < px)
                {
                    cout << "NO" << endl;
                    flag = false;
                    break;
                }

            if (xy[i].second != 0 || py != 0)
                if (xy[i].second < py)
                {
                    cout << "NO" << endl;
                    flag = false;
                    break;
                }
            px = xy[i].first;
            py = xy[i].second;
        }
        if (flag)
            cout << "YES" << endl;
        else
            continue;

        px = 0;
        py = 0;
        for (int i = 0; i < len; i++)
        {
            for (int j = px; j < xy[i].first; j++)
            {
                cout << "R";
            }
            px = xy[i].first;
            if (xy[i].second != py)
            {
                while (xy[i].second != py)
                {
                    cout << "U";
                    py++;
                }
                i--;
            }
        }
        cout << endl;
    }
}