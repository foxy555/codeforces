#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len, k;
    cin >> len >> k;
    int a = 1;
    cout << 1 << " ";
    int last = 1;
    vector<bool> remain(len, 1);
    remain[0] = 0;
    for (int i = 1; i < k; i++)
    {
        cout << (last + (k - i + 1) * a) << " ";
        last = (last + (k - i + 1) * a);
        remain[last - 1] = 0;
        if (a == 1)
            a = -1;
        else
            a = 1;
    }
    int print = 0;
    int i = 0;
    while (print != len - k)
    {
        if (remain[i])
        {
            cout << i + 1 << " ";
            print++;
        }
        i++;
    }
}