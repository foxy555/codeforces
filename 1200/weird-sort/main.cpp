#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;

    while (tc--)
    {
        int alen, plen;
        cin >> alen >> plen;

        vector<int> alist(alen);
        vector<int> plist(alen);
        for (int i = 0; i < alen; i++)
        {
            cin >> alist[i];
        }
        for (int i = 0; i < plen; i++)
        {
            int temp;
            cin >> temp;

            plist[temp - 1] = 1;
        }
        bool flag = true;
        while (flag)
        {
            flag = false;
            for (int i = 0; i < alen - 1; i++)
            {
                if (plist[i] && alist[i] > alist[i + 1])
                {
                    swap(alist[i], alist[i + 1]);
                    flag = true;
                }
            }
        }
        flag = true;
        for (int i = 0; i < alen - 1; i++)
        {
            if (alist[i] > alist[i + 1])
            {
                flag = false;
                break;
            }
        }

        if (flag)
            cout << "YES" << endl;
        else
        {
            cout << "NO" << endl;
        }
    }
}