#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;
    vector<int> numlist(len);
    for (int i = 0; i < len; i++)
    {
        cin >> numlist[i];
    }
    int visible = len;
    sort(numlist.begin(), numlist.end());
    vector<bool> memory(5005);
    for (int i = 0; i < len; i++)
    {
        for (int j = 0; j < len; j++)
        {
            if (numlist[i] < numlist[j] && !memory[j])
            {
                memory[j] = 1;
                visible--;
                break;
            }
        }
    }
    cout << visible << endl;
}