#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k;
        cin >> n >> k;

        int olddivision = k / n;
        int answer = olddivision + k;
        while (answer / n != olddivision)
        {
            olddivision = answer / n;
            answer = k + answer / n;
        }
        cout << answer << endl;
    }
}