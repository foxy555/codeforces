#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int n, a, b;
    cin >> n >> a >> b;

    string airports;
    cin >> airports;

    if (airports[a - 1] == airports[b - 1])
    {
        cout << 0 << endl;
    }
    else
    {
        cout << 1 << endl;
    }
}