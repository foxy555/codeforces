#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    string s, t;
    cin >> s >> t;
    if (s == t)
    {
        cout << "YES" << endl;
        return 0;
    }
    bool ok = true;
    int loc = -1;
    for (int i = 0; i < min(n, m); i++)
    {
        if (s[i] == '*')
        {
            loc = i;
            break;
        }
        if (s[i] != t[i])
        {
            ok = false;
            break;
        }
    }
    if (loc == -1)
        loc = s.find('*');
    if (loc == string::npos)
    {
        cout << "NO" << endl;
        return 0;
    }

    if (!ok)
    {
        cout << "NO" << endl;
        return 0;
    }
    string os = t.substr(0, loc);
    string o = s.substr(0, loc);
    if (os != o)
    {
        cout << "NO" << endl;
        return 0;
    }
    t.erase(0, loc);
    s.erase(0, loc + 1);
    if (s.length() > t.length())
    {
        cout << "NO" << endl;
        return 0;
    }
    if (s.length() == t.length())
    {
        if (s != t)
        {
            cout << "NO" << endl;
        }
        else
            cout << "YES" << endl;
        return 0;
    }

    t.erase(0, t.length() - s.length());
    if (t == s)
    {
        cout << "YES" << endl;
    }
    else
        cout << "NO" << endl;
}