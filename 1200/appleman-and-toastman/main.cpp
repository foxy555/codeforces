#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int len;
    cin >> len;
    vector<long long> numlist(len);
    vector<long long> sumlist(len);
    long long sum = 0;
    for (long long i = 0; i < len; i++)
    {
        cin >> numlist[i];
    }

    sort(numlist.begin(), numlist.end());
    for (long long i = len - 1; i >= 0; i--)
    {
        sum += numlist[i];
        sumlist[i] = sum;
    }
    sum = sumlist[0];
    long long score = sum;

    for (long long i = 1; i < len; i++)
    {
        score += sumlist[i] + numlist[i - 1];
    }
    cout << score << endl;
}