#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int lannum, len;
    cin >> lannum >> len;

    vector<int> lanterns(lannum);

    for(int i = 0; i < lannum; i++)
        cin >> lanterns[i];

    sort(lanterns.begin(), lanterns.end());
    double radius = max(lanterns[0], len - lanterns[lannum - 1]);
    for(int i = 0; i < lannum - 1; i++)
    {
        radius = max(radius, (lanterns[i + 1] - lanterns[i]) / (double)2.0);
    }
    cout.setf(ios::fixed, ios::floatfield);
    cout.setf(ios::showpoint);
    cout << radius << endl;
}
