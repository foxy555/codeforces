#include <bits/stdc++.h>

using namespace std;

vector<long long> factors(long long x)
{
    vector<long long> result;
    long long i = 1;
    while (i * i <= x)
    {
        if (x % i == 0)
        {
            result.push_back(i);
            if (x / i != i)
            {
                result.push_back(x / i);
            }
        }
        i++;
    }
    return result;
}
int main()
{
    long long n;
    cin >> n;
    if (n % (long long)(2) == 0)
    {
        cout << n / 2 << endl;
    }
    else
    {
        vector<long long> factor = factors(n);
        sort(factor.begin(), factor.end());
        cout << (n - factor[1]) / 2 + 1 << endl;
    }
}