#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int len;
    cin >> len;

    vector<int> numlist(len);

    for (int i = 0; i < len; i++)
        cin >> numlist[i];

    sort(numlist.begin(), numlist.end());
    long long total = 0;
    for (int i = 0; i < len; i++)
    {
        total += abs(numlist[i] - i - 1);
    }
    cout << total << endl;
}