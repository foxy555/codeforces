#include <bits/stdc++.h>

using namespace std;

int main()
{
    int len, nsmall;
    cin >> len >> nsmall;

    vector<int> numbers;

    for(int i = 0; i < len; i++)
    {
        int a;
        cin >> a;
        numbers.push_back(a);
    }
    sort(numbers.begin(), numbers.end());

    if(nsmall == 0)
    {
        if(numbers[0] - 1 != 0)
            cout << numbers[0] - 1 << endl;
        else
            cout << -1 << endl;
        return 0;
    }
    int ans = numbers[nsmall - 1];
    int smallnum = 0;
    for(int i = 0; i < len; i++)
    {
        if(numbers[i] <= ans)
            smallnum++;
    }
    if(smallnum != nsmall)
        cout << -1 << endl;
    else
        cout << ans << endl;

}
