#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int len;
    cin >> len;
    vector<int> numlist(len);
    for (int i = 0; i < len; i++)
    {
        cin >> numlist[i];
    }
    sort(numlist.begin(), numlist.end());

    int maxconsistent = 0;
    int con = 0;
    int startlocation = 0;
    for (int i = 1; i < len; i++)
    {
        if (numlist[i] - numlist[startlocation] > 5)
        {
            if (i - startlocation > maxconsistent)
            {
                maxconsistent = i - startlocation;
            }
            startlocation++;
            i--;
        }
    }
    if (len - 1 - startlocation > maxconsistent)
        maxconsistent = len - 1 - startlocation + 1;
    cout << max(maxconsistent, 1) << endl;
}