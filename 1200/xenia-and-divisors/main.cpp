#include <bits/stdc++.h>

using namespace std;
int min3(const int &a, const int &b, const int &c)
{
    return min(min(a, b), min(b, c));
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;

    vector<int> numlist(7);
    for (int i = 0; i < len; i++)
    {
        int a;
        cin >> a;
        numlist[a - 1]++;

        if (a == 5 || a == 7)
        {
            cout << -1 << endl;
            return 0;
        }
    }
    int m = min3(numlist[0], numlist[2], numlist[5]);
    numlist[0] -= m;
    numlist[2] -= m;
    numlist[5] -= m;
    int j = min3(numlist[0], numlist[1], numlist[5]);
    numlist[0] -= j;
    numlist[1] -= j;
    numlist[5] -= j;
    int k = min3(numlist[0], numlist[1], numlist[3]);
    numlist[0] -= k;
    numlist[1] -= k;
    numlist[3] -= k;

    for (int i = 0; i < 7; i++)
    {
        if (numlist[i] != 0)
        {
            cout << -1 << endl;
            return 0;
        }
    }
    for (int i = 0; i < j; i++)
    {
        cout << "1 2 6" << endl;
    }
    for (int i = 0; i < k; i++)
    {
        cout << "1 2 4" << endl;
    }
    for (int i = 0; i < m; i++)
    {
        cout << "1 3 6" << endl;
    }
}