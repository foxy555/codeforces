#include <bits/stdc++.h>

using namespace std;
int r(int n)
{
    if (n == 0)
        return 1;
    if (n < 10)
        return n;
    return max(r(n / 10) * (n % 10), r(n / 10 - 1) * 9);
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int number;
    cin >> number;
    cout << r(number) << endl;
}