#include <bits/stdc++.h>

using namespace std;

int main()
{
    int numrides, mrides, onep, mp;
    cin >> numrides >> mrides >> onep >> mp;

    if(mp / (double)mrides < (double)onep)
    {
        int price = floor(numrides / (double)mrides) * mp;
        price += min((int)(numrides - mrides * floor(numrides / (double)mrides)) * onep, mp);
        cout << price << endl;
    }
    else
        cout << numrides * onep << endl;
}
