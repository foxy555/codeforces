#include <bits/stdc++.h>

using namespace std;

int main()
{
    int x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;

    int distancex = abs(x1 - x2);
    int distancey = abs(y1 - y2);
    if (distancex != 0 && distancey != 0 && distancex != distancey)
    {
        cout << -1 << endl;
        return 0;
    }
    if (x1 == x2)
    {
        cout << x1 + distancey << " " << y1 << " " << distancey + x2 << " " << y2 << endl;
        return 0;
    }
    if (y1 == y2)
    {
        cout << x1 << " " << y1 + distancex << " " << x2 << " " << y2 + distancex << endl;
    }
    else
    {
        cout << x1 << " " << y2 << " " << x2 << " " << y1 << endl;
    }
}