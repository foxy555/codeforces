#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    vector<pair<int, int> > coords(3);
    for (int i = 0; i < 3; i++)
    {
        cin >> coords[i].first >> coords[i].second;
    }

    cout << 3 << endl;

    cout << coords[0].first + coords[1].first - coords[2].first << " " << coords[0].second + coords[1].second - coords[2].second << endl;
    cout << coords[0].first + coords[2].first - coords[1].first << " " << coords[0].second + coords[2].second - coords[1].second << endl;
    cout << coords[1].first + coords[2].first - coords[0].first << " " << coords[1].second + coords[2].second - coords[0].second << endl;
}
