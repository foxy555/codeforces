#include <bits/stdc++.h>

using namespace std;

int main()
{
    int len;
    cin >> len;

    vector<int> numlist(len);
    for (int i = 0; i < len; i++)
        cin >> numlist[i];

    sort(numlist.begin(), numlist.end());
    int counter = 0;
    for (int i = 0; i < len - 1; i++)
    {
        while (numlist[i] >= numlist[i + 1])
        {
            numlist[i + 1]++;
            counter++;
        }
    }
    cout << counter << endl;
}