#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;

    string s;
    cin >> s;
    for (int i = 0; i < len - 1; i++)
    {
        if (s[i] == '0' && s[i + 1] == '0' && i == 0)
        {
            cout << "NO" << endl;
            return 0;
        }
        if (s[i] == '1' && s[i + 1] == '1')
        {
            cout << "NO" << endl;
            return 0;
        }
        if (s[i] == '0' && i != '0' && s[i - 1] == '0' && s[i + 1] == '0')
        {
            cout << "NO" << endl;
            return 0;
        }
        if (s[i] == '0' && s[i + 1] == '0' && i == len - 2)
        {
            cout << "NO" << endl;
            return 0;
        }
    }
    if (s == "0")
        cout << "NO" << endl;
    else
        cout << "YES" << endl;
}