#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int k;
    cin >> k;

    if (k == 0)
    {
        cout << 1 << endl;
        return 0;
    }
    string ans = "";
    while (k != 0)
    {
        if (k - 2 >= 0)
        {
            ans += "8";
            k -= 2;
            continue;
        }
        if (k - 1 >= 0)
        {
            ans += "4";
            k--;
        }
    }
    if (ans.length() > 18)
    {
        long long a = 0;
        istringstream(ans) >> a;
        if (a > (long long)(pow(10, 18)))
        {
            cout << -1 << endl;
            return 0;
        }
    }
    cout << ans << endl;
}