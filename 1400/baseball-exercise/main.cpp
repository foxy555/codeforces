#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<ll> alist(n), blist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> alist[i];
    }
    for (int i = 0; i < n; ++i)
    {
        cin >> blist[i];
    }

    pair<ll, ll> ans{0, 0};
    for (int i = 0; i < n; ++i)
    {
        pair<ll, ll> temp{max(ans.first, ans.second + alist[i]),
                          max(ans.second, ans.first + blist[i])};
        ans = temp;
    }
    cout << max(ans.first, ans.second) << endl;
}
