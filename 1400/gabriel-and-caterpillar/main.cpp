#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int h1, h2, a, b;
    cin >> h1 >> h2 >> a >> b;

    if (a * 8 + h1 >= h2)
    {
        cout << 0;
        return 0;
    }
    if (a <= b)
    {
        cout << -1;
        return 0;
    }

    int difference = h2 - h1;
    cout << ((difference - 8 * a - 1) + (12 * (a - b))) / (12 * (a - b));
}
