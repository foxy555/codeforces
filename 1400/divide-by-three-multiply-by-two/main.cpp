#include "bits/stdc++.h"

#define ll long long

using namespace std;

int c(ll x)
{
    int cnt = 0;
    while (x % 3 == 0)
    {
        --cnt;
        x /= 3;
    }
    return cnt;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<pair<int, ll>> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].second;
        nlist[i].first = c(nlist[i].second);
    }

    sort(nlist.begin(), nlist.end());
    for (int i = 0; i < n; ++i)
    {
        cout << nlist[i].second << " ";
    }
}
