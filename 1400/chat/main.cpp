#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    vector<int> nlist(n + 1);
    for (int i = 1; i < n + 1; ++i)
    {
        cin >> nlist[i];
    }

    // vector<int> dp(1e5 + 10, 0);
    // vector<int> dp2(1e5 + 10, 0);
    vector<int> ans(n + 1, 0);
    for (int i = 1; i < n + 1; ++i)
    {
        if (nlist[i] == 0)
        {
            ans[i] = min(i + k, n) - max(1, i - k) + 1;
        }
        else
        {
            ans[i] = ans[nlist[i]] + min(n, i + k) -
                     max(max(i - k, 1), min(n, nlist[i] + k) + 1) + 1;
        }
    }
    for (int i = 1; i < n + 1; ++i)
    {
        cout << ans[i] << " ";
    }
}
