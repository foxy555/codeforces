#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    if (n >= m)
    {
        cout << n - m << endl;
        return 0;
    }
    int backup = m;
    int ans = 0;
    while (n < m)
    {
        if (m % 2 == 0)
        {
            m /= 2;
            ans++;
        }
        else
        {
            m++;
            ans++;
        }
    }
    ans += (n - m);
    cout << ans;
}
