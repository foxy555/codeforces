#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    unordered_map<string, unordered_set<string>> nmap;
    for (int i = 0; i < n; ++i)
    {
        string s;
        cin >> s;
        int a;
        cin >> a;

        for (int j = 0; j < a; ++j)
        {
            string str;
            cin >> str;
            nmap[s].insert(str);
        }
        bool erase = 1;
        while (erase)
        {
            erase = 0;
            for (auto j : nmap[s])
            {
                for (auto k : nmap[s])
                {
                    if (j == k)
                        continue;
                    int comlen = min(j.size(), k.size());
                    string js = j.substr(j.size() - comlen, comlen);
                    string ks = k.substr(k.size() - comlen, comlen);
                    if (js == ks)
                    {
                        if (j.size() > k.size())
                        {
                            nmap[s].erase(k);
                            erase = 1;
                            break;
                        }
                        else
                        {
                            nmap[s].erase(j);
                            erase = 1;
                            break;
                        }
                    }
                }
                if (erase)
                    break;
            }
        }
    }
    cout << nmap.size() << endl;
    for (auto i : nmap)
    {
        cout << i.first << " " << i.second.size() << " ";
        for (auto j : i.second)
        {
            cout << j << " ";
        }
        cout << endl;
    }
}
