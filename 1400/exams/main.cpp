#include <bits/stdc++.h>

#define ll long long

using namespace std;
bool sortbysec(const pair<int, int> &a, const pair<int, int> &b)
{
    return (a.second < b.second);
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<pair<int, int>> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i].first >> nlist[i].second;
    }

    sort(nlist.begin(), nlist.end());

    if (nlist.size() == 1)
    {
        cout << nlist.front().second << endl;
    }
    else
    {
        int ans = -1;
        for (int i = 0; i < n; i++)
        {
            if (ans <= nlist[i].second)
            {
                ans = nlist[i].second;
            }
            else
                ans = nlist[i].first;
        }
        cout << ans << endl;
    }
}
