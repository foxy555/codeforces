#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    vector<vector<bool>> board;
    vector<int> vcnt(m, 0);
    int total = 0;
    for (int i = 0; i < n; i++)
    {
        vector<bool> line(m);
        for (int j = 0; j < m; j++)
        {
            char c;
            cin >> c;
            if (c != '*')
                line[j] = 0;
            else
            {
                line[j] = 1;
                vcnt[j]++;
                total++;
            }
        }
        board.push_back(line);
    }

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            int sum = vcnt[i];
            if (board[j][i] == 1)
                sum--;
            for (int k = 0; k < m; k++)
            {
                if (board[j][k] == 1)
                    sum++;
                //if (sum + m - k < total)
                //    break;
            }
            if (sum == total)
            {
                cout << "YES" << endl;
                cout << j + 1 << " " << i + 1 << endl;
                return 0;
            }
        }
    }
    cout << "NO" << endl;
}
