#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        string s;
        cin >> s;

        int n = s.size();
        int zero = 0, one = 0;
        bool bit = 0;
        ll ans = 0;
        for (int i = 0; i < n; ++i) {
            if (s[i] - '0' == bit || s[i] == '?') {
                ++zero;
            } else {
                zero = 0;
            }
            if (s[i] - '0' == !bit || s[i] == '?') {
                ++one;
            } else {
                one = 0;
            }
            ans += max(zero, one);
            bit = !bit;
        }
        cout << ans << endl;
    }
}
