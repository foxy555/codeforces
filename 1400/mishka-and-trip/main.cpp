#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    vector<ll> nlist(n), klist(k);
    ll sum = 0;
    for (int i = 0; i < n; ++i) {
        cin >> nlist[i];
        sum += nlist[i];
    }

    for (int i = 0; i < k; ++i) {
        cin >> klist[i];
        --klist[i];
    }

    ll ans = 0;
    for (int i = 0; i < n - 1; ++i) {
        ans += nlist[i] * nlist[i + 1];
    }
    ans += nlist.back() * nlist.front();
    cerr << ans << endl;

    // a * (sum - nlist[a - 1] - nlist[a + 1])
    // subtract += a;
    ll subtract = 0;
    int temp = klist.front() - 1;
    if (klist.front() == 0) {
        temp = n - 1;
    }
    ans += (sum - nlist[temp] - nlist[(klist.front() + 1) % n] -
            nlist[klist.front()]) *
           nlist[klist.front()];
    cerr << ans << endl;
    subtract += nlist[klist.front()];
    for (int i = 1; i < k - 1; ++i) {
        ll tempsum = sum;
        if (klist[i] - klist[i - 1] == 1) {
            tempsum += nlist[klist[i - 1]];
        }
        ans += (tempsum - nlist[klist[i] - 1] - nlist[klist[i] + 1] - subtract -
                nlist[klist[i]]) *
               nlist[klist[i]];
        subtract += nlist[klist[i]];
        // cerr << ans << endl;
    }
    cerr << ans << endl;
    if (k != 1) {
        // edge case klist.back() + 1 == nlist.size() && klist.front() == 1
        if (klist.back() + 1 == nlist.size() && klist.front() == 0) {
            cerr << subtract << endl;
            ans += (sum - nlist[klist.back() - 1] - subtract -
                    nlist[klist.back()]) *
                   nlist[klist.back()];
        } else {
            ans +=
                (sum - nlist[klist.back() - 1] - nlist[(klist.back() + 1) % n] -
                 subtract - nlist[klist.back()]) *
                nlist[klist.back()];
        }
        if (klist.back() - klist[k - 2] == 1) {
            // add back the lost value
            ans += nlist[klist.back()] * nlist[klist[k - 2]];
        }
    }
    cout << ans << endl;
}

