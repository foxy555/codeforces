#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    vector<int> prime;
    for (int i = 2; i < 1000; i++)
    {
        bool ok = 1;
        for (int j = 2; j * j <= i; j++)
        {
            if (i % j == 0)
            {
                ok = 0;
                break;
            }
        }
        if (ok)
            prime.push_back(i);
    }
    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        vector<int> ans(n);
        vector<bool> used(n, 0);
        int cnt = 0;
        int j = 0;
        int color = 0;
        while (cnt != n)
        {
            if (j == prime.size())
                break;
            bool u = 0;
            for (int i = 0; i < n; i++)
            {
                if (nlist[i] % prime[j] == 0 && !used[i])
                {
                    u = 1;
                    used[i] = 1;
                    cnt++;
                    ans[i] = color + 1;
                }
            }
            if (u)
                color++;
            j++;
        }
        cout << color << endl;
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
