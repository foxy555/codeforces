#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> nlist(1e5 + 50);
    set<ll> occured;
    for (ll i = 0; i < n; ++i)
    {
        ll a;
        cin >> a;
        nlist[a] = occured.size();
        occured.insert(a);
    }
    ll sum = 0;
    for (auto i : nlist)
    {
        sum += i;
    }
    cout << sum;
}
