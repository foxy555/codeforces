#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<pair<int, int>> nlist(n - 1);

    for (int i = 0; i < n - 1; ++i)
    {
        cin >> nlist[i].first >> nlist[i].second;
    }
}
