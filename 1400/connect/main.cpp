#include "bits/stdc++.h"

#define ll long long

using namespace std;
set<pair<int, int>> ablock;
int n;
vector<vector<bool>> board;
void dfs(int x, int y)
{
    if (x < 0 || x >= n)
    {
        return;
    }
    if (y < 0 || y >= n)
    {
        return;
    }
    // cout << x << " " << y << endl;
    if (board[x][y])
    {
        if (ablock.count(make_pair(x, y)))
            return;
        ablock.insert(make_pair(x, y));
        dfs(x - 1, y);
        dfs(x + 1, y);
        dfs(x, y - 1);
        dfs(x, y + 1);
    }
    else
        return;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        board.push_back(vector<bool>(n, 0));
    }
    int r1, c1, r2, c2;
    cin >> r1 >> c1 >> r2 >> c2;
    r1--;
    c1--;
    r2--;
    c2--;

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            char c;
            cin >> c;
            board[i][j] = c == '0';
        }
    }
    dfs(r1, c1);
    auto a = ablock;
    ablock.clear();
    dfs(r2, c2);
    auto b = ablock;

    if (a == b)
    {
        cout << 0;
    }
    else
    {
        int ans = INT_MAX;
        for (auto i : a)
        {
            for (auto j : b)
            {
                ans =
                    min(ans, (i.first - j.first) * (i.first - j.first) +
                                 (i.second - j.second) * (i.second - j.second));
            }
        }
        cout << ans;
    }
}
