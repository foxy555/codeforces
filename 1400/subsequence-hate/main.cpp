#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        string s;
        cin >> s;
        int n = s.length();

        vector<pair<bool, int>> charlist;
        charlist.push_back(make_pair(s.front() - '0', 1));
        for (int i = 1; i < n; i++)
        {
            if (s[i] - '0' == charlist.back().first)
                charlist.back().second++;
            else
            {
                charlist.push_back(make_pair(s[i] - '0', 1));
            }
        }
        bool ok = 1;
        n = charlist.size();
        for (int i = 0; i < n - 2; i++)
        {
            if (charlist[i].first == 1 && charlist[i + 1].first == 0 &&
                charlist[i + 2].first == 1)
            {
                ok = 0;
                break;
            }
            if (charlist[i].first == 0 && charlist[i + 1].first == 1 &&
                charlist[i + 2].first == 0)
            {
                ok = 0;
                break;
            }
        }
        if (ok)
        {
            cout << 0 << endl;
            continue;
        }
        set<int> remove1;
        set<int> remove0;
        int temp1 = 0;
        int temp0 = 0;
        for (int i = 0; i < n - 1; i++)
        {
            if (charlist[i].first == 1)
                temp1 += charlist[i].second;
            else
                temp0 += charlist[i].second;
            int rest0 = 0, rest1 = 0;
            for (int j = i + 1; j < n; j++)
            {
                if (charlist[j].first == 1)
                    rest0 += charlist[j].second;
                else
                    rest1 += charlist[j].second;
            }
            remove0.insert(temp0 + rest0);
            remove1.insert(temp1 + rest1);
            // cout << temp0 << " " << temp1 << endl;
        }
        int removeall1 = 0;
        int removeall0 = 0;
        for (int i = 0; i < n; i++)
        {
            if (charlist[i].first == 1)
            {
                removeall1 += charlist[i].second;
            }
            else
                removeall0 += charlist[i].second;
        }
        cout << min({*remove0.begin(), *remove1.begin(), removeall0,
                     removeall1})
             << endl;
    }
}
