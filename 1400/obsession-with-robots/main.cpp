#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    set<pair<int, int>> points;
    int x, y;
    x = y = 0;
    int n = s.size();
    points.insert({x, y});
    // points.insert({x + 1, y});
    // points.insert({x - 1, y});
    // points.insert({x, y - 1});
    // points.insert({x, y + 1});
    for (int i = 0; i < n; ++i)
    {
        if (s[i] == 'L')
            x--;
        if (s[i] == 'R')
            x++;
        if (s[i] == 'U')
            y--;
        if (s[i] == 'D')
            y++;
        if (points.count(make_pair(x, y)))
        {
            cout << "BUG";
            return 0;
        }
        bool l, r, u, d;
        l = r = u = d = 0;
        if (s[i] == 'L')
        {
            l = points.count({x - 1, y});
            u = points.count({x, y - 1});
            d = points.count({x, y + 1});
        }
        if (s[i] == 'R')
        {
            r = points.count({x + 1, y});
            u = points.count({x, y - 1});
            d = points.count({x, y + 1});
        }
        if (s[i] == 'U')
        {
            l = points.count({x - 1, y});
            r = points.count({x + 1, y});
            u = points.count({x, y - 1});
        }
        if (s[i] == 'D')
        {
            l = points.count({x - 1, y});
            r = points.count({x + 1, y});
            d = points.count({x, y + 1});
        }
        if (l || r || d || u)
        {
            cout << "BUG" << endl;
            return 0;
        }
        points.insert({x, y});
    }
    cout << "OK";
}
