#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string a, b;
    cin >> a >> b;

    vector<int> alist(26, 0);
    vector<int> blist(26, 0);
    for (int i = 0; i < a.size(); ++i)
    {
        ++alist[a[i] - 'a'];
    }
    for (int i = 0; i < b.size(); ++i)
    {
        ++blist[b[i] - 'a'];
    }

    for (int i = 0; i < 26; ++i)
    {
        if (blist[i] > alist[i])
        {
            cout << "need tree" << endl;
            return 0;
        }
    }
    if (a.size() == b.size())
    {
        cout << "array" << endl;
        return 0;
    }

    int j = 0;
    for (int i = 0; i < a.size(); ++i)
    {
        if (a[i] == b[j])
        {
            ++j;
        }
        if (j == b.size())
        {
            cout << "automaton" << endl;
            return 0;
        }
    }
    cout << "both" << endl;
}

