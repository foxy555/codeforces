#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, x, y;
    cin >> n >> x >> y;

    vector<ll> ans(n);
    ll last = y - n + 1;
    if (last <= 0) {
        cout << -1 << endl;
        return 0;
    }
    if (last * last + n - 1 >= x) {
        for (int i = 0; i < n - 1; i++) {
            cout << 1 << '\n';
        }
        cout << last << endl;
    } else
        cout << -1 << endl;
}
