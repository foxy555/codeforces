#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<int> per;
    for (int i = 0; i < n; ++i)
    {
        per.push_back(i + 1);
    }
    vector<int> ansvec;
    int big = 0;
    do
    {
        int temp = 0;
        for (int i = 0; i < n; ++i)
        {
            for (int j = i; j < n; ++j)
            {
                int s = INT_MAX;
                for (int k = i; k <= j; ++k)
                {
                    s = min(s, per[k]);
                }
                temp += s;
            }
        }
        if (temp > big)
        {
            big = temp;
            ansvec = per;
        }
    } while (next_permutation(per.begin(), per.end()));
    sort(per.begin(), per.end());
    int cnt = 1;
    do
    {
        int temp = 0;
        for (int i = 0; i < n; ++i)
        {
            for (int j = i; j < n; ++j)
            {
                int s = INT_MAX;
                for (int k = i; k <= j; ++k)
                {
                    s = min(s, per[k]);
                }
                temp += s;
            }
        }
        if (temp == big)
        {
            if (cnt == m)
            {
                for (auto i : per)
                {
                    cout << i << " ";
                }
                return 0;
            }
            else
                cnt++;
        }
    } while (next_permutation(per.begin(), per.end()));
}
