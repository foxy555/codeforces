#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    n *= 2;
    vector<int> ans(n + 1);
    for (int i = 1; i <= n; i++)
    {
        ans[i] = i;
    }

    for (int i = 1; i < n - k * 2; i += 2)
    {
        swap(ans[i], ans[i + 1]);
    }
    for (int i = 1; i <= n; i++)
    {
        cout << ans[i] << " ";
    }
}
