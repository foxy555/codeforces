#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int a, x, y;
    cin >> a >> x >> y;

    if (y % a == 0)
    {
        cout << -1;
        return 0;
    }

    int ans = 0;
    for (int i = 0; i < 1e6; ++i)
    {
        if (i % 2 == 0 && i != 0)
            ++ans;
        ++ans;
        if (i * a < y && (i + 1) * a > y)
        {
            if (i % 2 == 0 && i != 0)
            {
                if (x == 0)
                {
                    cout << -1;
                    break;
                }
                if (abs(x) >= a)
                {
                    cout << -1;
                    break;
                }
                if (x < 0)
                {
                    cout << ans - 1;
                }
                else
                    cout << ans;
                break;
            }
            else
            {
                if (abs(x) >= (a + 1) / 2)
                    cout << -1;
                else
                    cout << ans;
                break;
            }
        }
    }
}
