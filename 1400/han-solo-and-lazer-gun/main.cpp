#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<pair<int, int>> nlist(n);
    pair<double, double> loc;
    map<double, bool> nmap;
    cin >> loc.first >> loc.second;

    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].first >> nlist[i].second;
    }

    int ans = 0;
    for (int i = 0; i < n; ++i)
    {
        if (loc.first - nlist[i].first == 0)
        {
            ans = 1;
        }
        else
        {
            nmap[(loc.second - nlist[i].second) /
                 (loc.first - nlist[i].first)] = true;
        }
    }
    cout << nmap.size() + ans << endl;
}
