#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, s;
    cin >> n >> s;
    if (n * 2 > s)
    {
        cout << "NO" << endl;
        return 0;
    }

    cout << "YES" << endl;
    for (int i = 0; i < n - 1; ++i)
    {
        cout << 1 << " ";
    }
    cout << s - n + 1 << endl;
    cout << s / 2 << endl;
}
