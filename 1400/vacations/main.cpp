#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    int ans = 0;
    for (int i = 0; i < n; i++)
    {
        if (nlist[i] == nlist[i - 1] && nlist[i] != 3)
            nlist[i] = 0;
        if (nlist[i] != nlist[i - 1] && nlist[i - 1] == 1 && nlist[i] == 3)
            nlist[i] = 2;
        if (nlist[i] != nlist[i - 1] && nlist[i - 1] == 2 && nlist[i] == 3)
            nlist[i] = 1;
        if (nlist[i] == 0)
            ans++;
    }
    cout << ans << endl;
}
