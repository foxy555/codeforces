#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int num = 1e6;
    vector<ll> dlist(num);
    ll high = 0;
    for (int i = 0; i < n; ++i)
    {
        int a;
        cin >> a;
        dlist[a - i + n] += a;
        high = max(high, dlist[a - i + n]);
    }
    cout << high << endl;
}
