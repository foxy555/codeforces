#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int m, s;
    cin >> m >> s;
    if (m == 1 && s < 10)
    {
        cout << s << " " << s << endl;
        return 0;
    }
    if (s == 0)
    {
        cout << -1 << " " << -1 << endl;
        return 0;
    }

    vector<int> number1(m, 0);
    number1.front() = 1;
    int left = s - 1;
    for (int i = m - 1; i >= 1; i--)
    {
        number1[i] = min(9, left);
        left -= min(9, left);
        if (left == 0)
            break;
    }
    if (left != 0)
    {
        while (number1.front() != 9)
        {
            number1.front()++;
            left--;
            if (left == 0)
                break;
        }
        if (left != 0)
        {
            cout << -1 << " " << -1 << endl;
            return 0;
        }
    }
    vector<int> number2(m, 0);
    left = s;
    for (int i = 0; i < m; i++)
    {
        number2[i] = min(9, left);
        left -= min(9, left);
        if (left == 0)
            break;
    }
    if (left != 0)
    {
        cout << -1 << " " << -1 << endl;
    }
    else
    {
        for (auto i : number1)
            cout << i;
        cout << " ";
        for (auto i : number2)
            cout << i;
    }
}
