#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<ll> nlist(n);
        ll x = 0;
        ll sum = 0;
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
            sum += nlist[i];
            x ^= nlist[i];
        }

        cout << 2 << endl << x << " " << x + sum << endl;
    }
}
