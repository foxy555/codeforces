#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    if (n < 3)
    {
        cout << -1;
        return 0;
    }
    if (n == 3)
    {
        cout << 210;
        return 0;
    }
    vector<int> pattern{160, 130, 40, 190, 10, 100};
    // starts at n > 3
    string last = to_string(abs(210 - pattern[(n - 4) % 6]));
    cout << 1;
    for (int i = 1; i < n - last.size(); ++i)
    {
        cout << 0;
    }
    cout << last;
}
