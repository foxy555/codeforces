#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int z, o;
    cin >> z >> o;
    int oldz = z, oldo = o;
    string ans = "";
    if (z == o)
    {
        for (int i = 0; i < z; i++)
        {
            cout << 10;
        }
        return 0;
    }
    if (z - 1 == o)
    {
        cout << 0;
        for (int i = 0; i < o; i++)
        {
            cout << 10;
        }
        return 0;
    }
    while (o != z)
    {
        ans += "110";
        o -= 2;
        z -= 1;
        if (z == 0)
            break;
    }
    if (o != z)
    {
        if (o == 1)
        {
            ans += "1";
            cout << ans << endl;
            return 0;
        }
        if (o == 2)
        {
            ans += "11";
            cout << ans << endl;
            return 0;
        }
        cout << -1 << endl;
        return 0;
    }
    for (int i = 0; i < o; i++)
    {
        ans += "10";
    }
    int one = 0, zero = 0;
    for (int i = 0; i < ans.length(); i++)
    {
        if (ans[i] == '1')
            one++;
        else
            zero++;
    }
    if (one != oldo || zero != oldz)
    {
        cout << -1 << endl;
    }
    else
        cout << ans << endl;
}
