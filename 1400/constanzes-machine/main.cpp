#include "bits/stdc++.h"

#define ll long long

using namespace std;
const int MOD = 1e9 + 7;
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    vector<ll> fib(1e5 + 5, 0);
    fib[0] = fib[1] = 1;
    for (int i = 2; i < fib.size(); ++i)
    {
        fib[i] = fib[i - 1] + fib[i - 2];
        fib[i] %= MOD;
    }
    vector<pair<char, int>> nlist;
    nlist.push_back({s[0], 0});
    for (int i = 0; i < s.size(); ++i)
    {
        if (nlist.back().first == s[i])
            ++nlist.back().second;
        else
            nlist.push_back({s[i], 1});
        if (s[i] == 'm' || s[i] == 'w')
        {
            cout << 0 << endl;
            return 0;
        }
    }
    ll ans = 1;
    for (int i = 0; i < nlist.size(); ++i)
    {
        if (nlist[i].first > 1)
            if (nlist[i].first == 'n' || nlist[i].first == 'u')
            {
                ans *= fib[nlist[i].second];
                ans %= MOD;
            }
    }
    cout << ans << endl;
}
