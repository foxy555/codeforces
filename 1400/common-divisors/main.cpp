#include "bits/stdc++.h"

using namespace std;
string repeat(int n, string s)
{
    ostringstream os;
    for(int i = 0; i < n; i++)
        os << s;
    return os.str();

}
int main()
{
    string a,b;
    cin >> a >> b;

    vector<string>  aprefix;
    for(int i = 0; i < a.size(); ++i)
    {
        if(a.size() % (i + 1) == 0)
        {
            string prefix = a.substr(0,i + 1);
            if(repeat(a.size() / (i + 1) , prefix) == a)
                aprefix.push_back(a.substr(0,i + 1));
        }

    }
    vector<string> bprefix;
    for(int i = 0; i < b.size(); ++i)
    {
        if(b.size() % (i + 1) == 0)
        {
            string prefix = b.substr(0,i + 1);
            if(repeat(b.size() / (i + 1) , prefix) == b)
                bprefix.push_back(b.substr(0,i + 1));
        }
    }
    int ans = 0;
    int i = 0,j = 0;
    while(i < aprefix.size() && j < bprefix.size())
    {
        if(aprefix[i].size() < bprefix[j].size())
        {
            i++;
            continue;
        }
        if(aprefix[i].size() > bprefix[j].size())
        {
            j++;
            continue;
        }
        if(aprefix[i].size() == bprefix[j].size())
        {
            if(aprefix[i] == bprefix[j])
                ans++;
            ++i;
            ++j;
        }
    }
    cout << ans;

}
