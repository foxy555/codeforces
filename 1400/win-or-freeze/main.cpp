#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    ll s = 0;
    for (ll i = 2; i * i < n; ++i)
    {
        if (n % i == 0 && !s)
        {
            s = i;
            n /= i;
        }
        if (n % i == 0 && n > i)
        {
            cout << 1 << endl << s * i;
            return 0;
        }
    }
    if (!s)
    {
        cout << 1 << endl << 0;
    }
    else
        cout << 2;
}
