#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int a, b, n;
    cin >> a >> b >> n;
    if (a == 0 && b == 0)
    {
        cout << 0;
        return 0;
    }
    if (a == 0)
    {
        cout << "No solution";
        return 0;
    }
    int q = b / a;
    if (q != (double)b / a)
    {
        cout << "No solution";
        return 0;
    }

    for (int i = -10000; i < 10000; ++i)
    {
        if (pow(i, n) == q)
        {
            cout << i;
            return 0;
        }
    }
    cout << "No solution";
    return 0;
}
