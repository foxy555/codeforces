#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int m = 1e5 + 5;
    vector<int> nlist(1e5 + 5, 0);
    for (int i = 0; i < n; ++i)
    {
        int a;
        cin >> a;

        ++nlist[a];
    }

    int cnt4 = 0;
    int cnt2 = 0;
    for (int i = 0; i < m; ++i)
    {
        cnt4 += nlist[i] / 4;
        cnt2 += nlist[i] / 2;
    }

    int q;
    cin >> q;
    while (q--)
    {
        char c;
        int a;
        cin >> c >> a;
        cnt4 -= nlist[a] / 4;
        cnt2 -= nlist[a] / 2;
        if (c == '-')
        {
            --nlist[a];
        }
        else
        {
            ++nlist[a];
        }

        cnt4 += nlist[a] / 4;
        cnt2 += nlist[a] / 2;

        if (cnt4 >= 1 && cnt2 >= 4)
        {
            cout << "YES" << endl;
        }
        else
        {
            cout << "NO" << endl;
        }
    }
}
