#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int x, y, arr[1000], ar1, ar2, sum = 0;
    cin >> x >> y;
    for (int i = 0; i < x; i++)
        cin >> arr[i];
    for (int i = 0; i < y; i++)
    {
        cin >> ar1 >> ar2;
        sum += min(arr[ar1 - 1], arr[ar2 - 1]);
    }
    cout << sum;
    return 0;
}
