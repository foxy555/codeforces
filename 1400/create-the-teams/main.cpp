#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, x;
        cin >> n >> x;

        vector<int> nlist(n);

        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        sort(nlist.rbegin(), nlist.rend());
        int ans = 0;
        int cnt = 1;
        for (int i = 0; i < n; ++i)
        {
            if (nlist[i] * cnt >= x)
            {
                ans++;
                cnt = 0;
            }
            ++cnt;
        }
        cout << ans << endl;
    }
}
