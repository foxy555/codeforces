#include "bits/stdc++.h"

#define ll long long

using namespace std;

vector<int> factors(int x)
{
    vector<int> result;
    for (int i = 1; i * i <= x; ++i)
    {
        if (x % i == 0)
        {
            result.push_back(i);
            if (x / i != i)
            {
                result.push_back(x / i);
            }
        }
    }
    return result;
}
const ll mod = 1e9 + 7;
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    ll ans = 0;
    vector<vector<ll>> dp(n + 1, vector<ll>(k + 1, 0));
    for (int i = 1; i <= k; ++i)
    {
        dp[1][i] = 1;
    }
    for (int i = 2; i <= n; ++i)
    {
        vector<int> factor = factors(i);
        if (factor.size() == 2)
        {
            for (int j = 1; j <= k; ++j)
            {
                dp[i][j] = j % mod;
            }
        }
        else
        {
            dp[i][1] = 1;
            for (int j = 2; j <= k; ++j)
            {
                dp[i][j] = dp[i][j - 1] + 1;
                for (int l = 2; l < factor.size(); ++l)
                {
                    dp[i][j] += (dp[factor[l]][j - 1]) % mod;
                }
            }
        }
    }
    for (int i = 1; i <= n; ++i)
    {
        ans += dp[i][k] % mod;
    }
    cout << ans % mod << endl;
}
