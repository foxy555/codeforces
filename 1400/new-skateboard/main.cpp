#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    int n = s.length();
    ll ans = 0;
    for (ll i = 0; i < n; ++i)
    {
        if ((s[i] - '0') % 4 == 0)
            ++ans;
        if (i != 0)
        {
            if (((s[i - 1] - '0') * 10 + (s[i] - '0')) % 4 == 0)
                ans += i;
        }
    }
    cout << ans << endl;
}
