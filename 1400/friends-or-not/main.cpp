#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, d;
    cin >> n >> d;

    vector<pair<string, string>> nlist(n);
    vector<int> tlist(n);

    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].first >> nlist[i].second >> tlist[i];
    }

    set<pair<string, string>> friends;

    for (int i = 0; i < n; ++i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            if (nlist[i].first == nlist[j].second &&
                nlist[i].second == nlist[j].first && tlist[j] != tlist[i] &&
                tlist[j] - tlist[i] <= d)
            {
                friends.insert(make_pair(min(nlist[i].first, nlist[i].second),
                                         max(nlist[i].first, nlist[i].second)));
            }
        }
    }
    cout << friends.size() << endl;
    for (auto i : friends)
    {
        cout << i.first << " " << i.second << endl;
    }
}
