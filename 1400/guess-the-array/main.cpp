#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist(n);

    int ot, tt, to;
    ot = tt = to = 0;

    cout << "? 1 2" << endl;
    fflush(stdout);
    cin >> ot;
    cout << "? 2 3" << endl;
    fflush(stdout);
    cin >> tt;
    cout << "? 3 1" << endl;
    fflush(stdout);
    cin >> to;

    nlist[0] = ot - tt + to;
    nlist[0] /= 2;
    nlist[1] = ot - nlist.front();
    nlist[2] = to - nlist.front();
    for (int i = 3; i < n; ++i)
    {
        int a;
        cout << "? 1 " << i + 1 << endl;
        cin >> a;
        fflush(stdout);
        nlist[i] = a - nlist.front();
    }
    cout << "! ";
    fflush(stdout);
    for (auto i : nlist)
    {
        cout << i << " ";
        fflush(stdout);
    }
    cout << endl;
    fflush(stdout);
}
