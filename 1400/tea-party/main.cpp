#include "bits/stdc++.h"

#define ll long long

using namespace std;
bool sortbysec(const pair<int, int> &a, const pair<int, int> &b)
{
    return (a.second < b.second);
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, w;
    cin >> n >> w;

    vector<pair<int, int>> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].first;
        nlist[i].second = i;
    }

    sort(nlist.begin(), nlist.end());

    vector<int> anslist(n);
    for (int i = 0; i < n; ++i)
    {
        w -= (nlist[i].first + 1) / 2;
        anslist[nlist[i].second] = (nlist[i].first + 1) / 2;
        if (w < 0)
        {
            cout << -1;
            return 0;
        }
    }
    sort(nlist.rbegin(), nlist.rend());
    if (w != 0)
    {
        for (int i = 0; i < n; ++i)
        {
            int pour = min(w, nlist[i].first - anslist[nlist[i].second]);
            w -= pour;
            anslist[nlist[i].second] += pour;
        }
    }
    for (auto i : anslist)
    {
        cout << i << " ";
    }
}
