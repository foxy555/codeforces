#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll a, b, c;
    cin >> a >> b >> c;

    ll m;
    cin >> m;

    vector<ll> usb, ps2;
    for (ll i = 0; i < m; ++i)
    {
        ll t;
        cin >> t;
        string s;
        cin >> s;
        if (s.front() == 'U')
            usb.push_back(t);
        else
            ps2.push_back(t);
    }
    sort(usb.rbegin(), usb.rend());
    sort(ps2.rbegin(), ps2.rend());

    ll ans1 = 0;
    ll ans1ct = 0;
    // ll ab, bb, cb;
    // ab = a;
    // bb = b;
    // cb = c;

    // vector<ll> u = usb;
    // vector<ll> p = ps2;

    while (min(((ll)usb.size()), a))
    {
        a--;
        ans1 += usb.back();
        ans1ct++;
        usb.pop_back();
    }
    while (min((ll)(ps2.size()), b))
    {
        b--;
        ans1 += ps2.back();
        ans1ct++;
        ps2.pop_back();
    }
    while (max(ps2.size(), usb.size()) && c)
    {
        c--;
        ll us = INT_MAX, ps = INT_MAX;
        if (usb.size())
        {
            us = usb.back();
        }
        if (ps2.size())
        {
            ps = ps2.back();
        }
        if (us > ps)
        {
            ps2.pop_back();
            ans1ct++;
            ans1 += ps;
        }
        else
        {
            usb.pop_back();
            ans1ct++;
            ans1 += us;
        }
    }
    cout << ans1ct << " " << ans1;
}
