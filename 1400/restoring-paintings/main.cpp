#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, a, b, c, d;
    cin >> n >> a >> b >> c >> d;

    ll ans = 0;
    for (ll x = 1; x <= n; ++x)
    {
        ll y = x + b - c;
        ll z = x + a - d;
        ll w = a + y - d;
        if (1 <= y && y <= n && 1 <= z && z <= n && 1 <= w && w <= n)
        {
            ans++;
        }
    }
    cout << ans * n << endl;
}
