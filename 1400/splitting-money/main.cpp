#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> nlist(n);
    for (ll i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    ll x, f;
    cin >> x >> f;

    ll ans = 0;
    for (ll i = 0; i < n; ++i)
    {
        if (nlist[i] > x)
        {
            ans += (nlist[i] + f - 1) / (f + x) * f;
        }
    }
    cout << ans;
}
