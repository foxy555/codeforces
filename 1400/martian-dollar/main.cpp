#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, b;
    cin >> n >> b;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    int ans = 0;
    for (int i = n - 1; i >= 0; --i)
    {
        int small = INT_MAX;
        for (int j = 0; j < i; ++j)
        {
            small = min(nlist[j], small);
        }
        ans = max(ans, b - (b / small) * small + nlist[i] * (b / small));
    }
    cout << ans;
}
