#include "bits/stdc++.h"
#include <atomic>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, u, r, d, l;
        cin >> n >> u >> r >> d >> l;
        bool ok = 0;
        for (int i = 0; i < 16; ++i)
        {
            int bu = u, br = r, bd = d, bl = l;
            if (i & 1)
            {
                --bu;
                --bl;
            }
            if (i & 2)
            {
                --bu;
                --br;
            }
            if (i & 4)
            {
                --br;
                --bd;
            }
            if (i & 8)
            {
                --bl;
                --bd;
            }
            if (min({bu, br, bd, bl}) >= 0 && max({bu, br, bd, bl}) <= n - 2)
            {
                ok = 1;
                goto i_have_won__but_at_what_cost;
            }
        }
    i_have_won__but_at_what_cost:
        if (ok)
        {
            cout << "YES" << endl;
        }
        else
        {
            cout << "NO" << endl;
        }
    }
}
