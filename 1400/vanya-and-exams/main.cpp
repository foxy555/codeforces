#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, r, avg;
    cin >> n >> r >> avg;
    ll sum = 0;
    vector<pair<ll, ll>> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].second >> nlist[i].first;
        sum += nlist[i].second;
    }
    sort(nlist.begin(), nlist.end());
    ll i;
    ll rez = i = 0;
    while (sum < avg * n)
    {
        long long tmp = min(avg * n - sum, r - nlist[i].second);
        rez += tmp * nlist[i].first;
        sum += tmp;
        i++;
    }

    cout << rez << endl;
}
