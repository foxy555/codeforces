#include <bits/stdc++.h>
#include <vector>

#define ll long long

using namespace std;

bool check(string t, string s)
{
    int j = 0;
    for (int i = 0; i < t.size(); i++)
    {
        bool ok = 0;
        while (j < s.size())
        {
            if (s[j] == t[i])
            {
                ok = 1;
                j++;
                break;
            }
            j++;
            // cout << s[j] << " " << t[i] << endl;
        }
        if (!ok)
            return false;
    }
    return true;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string s = to_string(n);
    if (int(sqrt(n)) == sqrt(n))
    {
        cout << 0 << endl;
        return 0;
    }
    int ans = -1;
    for (int i = 1; i <= 44722; i++)
    {
        string t = to_string(i * i);
        if (t.size() > s.size())
            break;

        bool ok = check(t, s);
        if (ok)
        {
            ans = t.length();
        }
    }
    if (ans == -1)
        cout << -1;
    else
        cout << s.length() - ans << endl;
}
