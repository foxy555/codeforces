#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        set<int> ans;
        for (int i = 0; i * i <= n; ++i)
        {
            ans.insert(i);
        }
        for (int i = 1; i * i <= n; ++i)
        {
            ans.insert(n / i);
        }
        cout << ans.size() << endl;
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
