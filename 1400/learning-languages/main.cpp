#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    // vector<set<int>> nlist;
    // map<int, set<int>> lan;
    vector<set<int>> knownlang;
    int ans = 0;
    for (int i = 0; i < n; ++i)
    {
        int a;
        cin >> a;
        if (a != 0)
            knownlang.push_back(set<int>());
        else
            ans++;
        for (int j = 0; j < a; ++j)
        {
            int t;
            cin >> t;
            // lan[t].insert(i);
            knownlang.back().insert(t);
        }
    }
    // for (int i = 0; i < n; ++i)
    //{
    //    bool ne = true;
    //    for (int k = 0; k < knownlang.size(); ++k)
    //    {
    //        for (auto l : nlist[i])
    //        {
    //            if (knownlang[k].count(l))
    //            {
    //                knownlang[k].insert(nlist[i].begin(), nlist[i].end());
    //                ne = false;
    //                break;
    //            }
    //        }
    //    }
    //    if (ne)
    //        knownlang.push_back(nlist[i]);
    //}
    int oldsize = knownlang.size();
    while (true)
    {
        for (int i = 0; i < knownlang.size(); ++i)
        {
            vector<int> epos;
            for (int j = i + 1; j < knownlang.size(); ++j)
            {
                for (auto k : knownlang[i])
                {
                    if (knownlang[j].count(k))
                    {
                        epos.push_back(j);
                        knownlang[i].insert(knownlang[j].begin(),
                                            knownlang[j].end());
                        break;
                    }
                }
            }
            for (int j = 0; j < epos.size(); ++j)
            {
                knownlang.erase(knownlang.begin() + epos[j] - j);
            }
            if (epos.size())
                break;
        }
        if (knownlang.size() == oldsize)
            break;
        oldsize = knownlang.size();
    }
    if (knownlang.size() >= 1)
        cout << knownlang.size() + ans - 1 << endl;
    else
        cout << ans << endl;
}
