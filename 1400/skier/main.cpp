#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;
        int n = s.length();

        set<pair<pair<int, int>, pair<int, int>>> travel_hist;
        int x = 0, y = 0;
        int lastx, lasty;
        int ans = 0;
        for (int i = 0; i < n; i++)
        {
            lastx = x;
            lasty = y;
            switch (s[i])
            {
            case 'N':
                y--;
                break;
            case 'S':
                y++;
                break;
            case 'E':
                x++;
                break;
            case 'W':
                x--;
                break;
            }
            // cout << x << " " << y << endl;
            if (travel_hist.count(
                    make_pair(make_pair(lastx, lasty), make_pair(x, y))))
            {
                ans++;
            }
            else
            {
                travel_hist.insert(
                    make_pair(make_pair(lastx, lasty), make_pair(x, y)));
                travel_hist.insert(
                    make_pair(make_pair(x, y), make_pair(lastx, lasty)));
                ans += 5;
            }
        }
        cout << ans << endl;
    }
}
