#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, p, k;
        cin >> n >> p >> k;

        vector<int> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        sort(nlist.begin(), nlist.end());

        if (nlist.front() > p)
        {
            cout << 0 << endl;
            continue;
        }

        int ans1 = 1, ans2 = 0;
        int p1, p2;
        p1 = p2 = p;
        p1 -= nlist.front();
        for (int i = 1; i < n; ++i)
        {
            if (i % 2 == 0)
            {
                if (p1 - nlist[i] >= 0)
                {
                    p1 -= nlist[i];
                    ans1 += 2;
                }
            }
            else
            {
                if (p2 - nlist[i] >= 0)
                {
                    p2 -= nlist[i];
                    ans2 += 2;
                }
            }
        }
        cout << max(ans1, ans2) << endl;
    }
}
