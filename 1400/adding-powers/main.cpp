#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k;
        cin >> n >> k;
        vector<ll> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }
        sort(nlist.rbegin(), nlist.rend());

        int high = 0;
        while (true)
        {
            if (pow(k, high) < 1e16)
            {
                ++high;
            }
            else
                break;
        }

        if (nlist.front() == 0)
        {
            cout << "YES" << endl;
            continue;
        }
        vector<bool> used(100, false);
        vector<bool> good(n, false);
        bool ok = true;
        for (int i = 0; i < n; ++i)
        {
            if (nlist[i] == 0)
                continue;
            ll temp = 0;
            for (int j = high; j >= 0; --j)
            {
                if (!used[j])
                {
                    if (pow(k, j) + temp == nlist[i])
                    {
                        used[j] = true;
                        good[i] = true;
                        break;
                    }
                    if (pow(k, j) + temp < nlist[i])
                    {
                        temp += pow(k, j);
                        used[j] = true;
                    }
                }
            }
            if (!good[i])
            {
                ok = false;
                break;
            }
        }
        if (ok)
        {
            cout << "YES" << endl;
        }
        else
            cout << "NO" << endl;
    }
}
