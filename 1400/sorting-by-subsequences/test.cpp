#include<bits/stdc++.h>
using namespace std;
 
int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	int n;
	cin>>n;
 
	vector<pair<int,int>>foo(n);
	for(int i=0;i<n;i++){
		int x;
		cin>>x;
		foo[i]={x,i};
	}
	sort(foo.begin(),foo.end());
 
 
	vector<int>adj(n);
	
	for(int i=0;i<n;i++){
		int u = foo[i].second;
		int v = i;
		adj[u]=v;
		//adj[v]=u;
 
	}
	vector<int>colors(n,-1);
	int id=0;
	for(int i=0;i<n;i++){
		if(colors[i]!=-1)continue;
 
		colors[i]=++id;
		int u = i;
		int v = adj[u];
		while(v!=u){
			colors[v]=id;
			v=adj[v];
		}
	}
	
	vector<vector<int>>ans(id+1);
	for(int i=0;i<n;i++){
		ans[colors[i]].push_back(i+1);
	}
	cout<<id<<'\n';
 
	for(int i=1;i<=id;i++){
		cout<<ans[i].size()<<' ';
		for(int j:ans[i])cout<<j<<' ';
		cout<<'\n';
	}
 
	
 
	return 0;
}	