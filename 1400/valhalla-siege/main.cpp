#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, q;
    cin >> n >> q;

    vector<ll> solder(n + 1, 0);
    ll pre = 0;
    for (int i = 1; i <= n; ++i)
    {
        ll a;
        cin >> a;
        solder[i] = pre + a;
        pre += a;
    }

    ll temp = 0;
    ll damage;
    for (int tc = 0; tc < q; ++tc)
    {
        cin >> damage;

        temp += damage;
        auto it = upper_bound(solder.begin(), solder.end(), temp);
        if (it == solder.end())
        {
            cout << n << endl;
            temp = 0;
        }
        else
        {
            cout << n - int(it - solder.begin()) + 1 << endl;
        }
    }
}
