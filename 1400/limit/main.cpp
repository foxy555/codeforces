#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<int> nlist(n + 1), mlist(m + 1);
    for (int i = 0; i < n + 1; ++i)
    {
        cin >> nlist[i];
    }
    for (int i = 0; i < m + 1; ++i)
    {
        cin >> mlist[i];
    }
    if (m > n)
    {
        cout << "0/1";
        return 0;
    }
    if (n > m)
    {
        if (nlist.front() > 0 && mlist.front() > 0 ||
            nlist.front() < 0 && mlist.front() < 0)
            ;
        else
            cout << '-';
        cout << "Infinity";
        return 0;
    }
    if (nlist.front() > 0 && mlist.front() > 0 ||
        nlist.front() < 0 && mlist.front() < 0)
        ;
    else
        cout << '-';
    cout << abs(nlist.front() / __gcd(nlist.front(), mlist.front())) << "/"
         << abs(mlist.front() / __gcd(nlist.front(), mlist.front()));
}
