#include "bits/stdc++.h"

#define ll long long

using namespace std;

vector<int> parent(5e5 + 5);
vector<int> size(5e5 + 5, 1);
int getParent(int node) {
    if (node == parent[node]) return node;
    return parent[node] = getParent(parent[node]);
}
void unionSets(int a, int b) {
    a = getParent(a);
    b = getParent(b);
    if (a == b) return;
    if (size[a] < size[b]) swap(a, b);
    parent[b] = a;
    size[a] += size[b];
}
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    for (int i = 0; i < parent.size(); ++i) {
        parent[i] = i;
    }

    for (int i = 0; i < m; ++i) {
        int k;
        cin >> k;
        if (k == 0) continue;
        int prev;
        cin >> prev;
        for (int j = 1; j < k; ++j) {
            int a;
            cin >> a;
            unionSets(prev, a);
            prev = a;
        }
    }
    for (int i = 0; i < n; ++i) {
        cout << size[getParent(i + 1)] << " ";
    }
}
