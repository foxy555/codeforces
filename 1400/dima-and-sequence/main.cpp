#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    map<ll, ll> nmap;
    for (ll i = 0; i < n; i++)
    {
        ll a;
        cin >> a;
        nmap[__builtin_popcount(a)]++;
    }

    ll ans = 0;
    for (auto i : nmap)
    {
        ans += (i.second * (i.second - 1)) / 2;
    }
    cout << ans << endl;
}
