#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    if (s.size() < 4)
    {
        cout << "No";
        return 0;
    }

    vector<int> letters(26, 0);
    for (int i = 0; i < s.size(); ++i)
    {
        letters[s[i] - 'a']++;
    }
    int distinct = 0;
    for (int i = 0; i < letters.size(); ++i)
    {
        distinct += letters[i];
    }

    if (distinct > 4)
    {
        cout << "No";
        return 0;
    }
    if (distinct == 4 && distinct == 3)
    {
        cout << "Yes";
        return 0;
    }
    if (distinct == 2)
    {
    }
}
