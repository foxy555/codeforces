#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    string s;
    cin >> s;

    if (n == 1)
    {
        cout << 0 << endl << s << endl;
        return 0;
    }

    int ans = 0;
    vector<char> colors{'B', 'G', 'R'};
    for (int i = 1; i < n - 1; ++i)
    {
        if (s[i] == s[i - 1])
        {
            int a = -1, b = -1, c = -1;
            for (int j = 0; j < 3; ++j)
            {
                if (colors[j] == s[i - 1])
                    a = j;
                if (colors[j] == s[i + 1])
                    b = j;
                if (j != a && j != b)
                {
                    c = j;
                    break;
                }
            }
            s[i] = colors[c];
            ans++;
        }
    }
    if (s[n - 1] == s[n - 2])
    {
        ans++;
        if (s[n - 2] == 'G')
        {
            s[n - 1] = 'B';
        }
        else
            s[n - 1] = 'G';
    }
    cout << ans << endl << s << endl;
}
