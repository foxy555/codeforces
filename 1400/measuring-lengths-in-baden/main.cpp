#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    int inches = n / 3;
    if (n % 3 == 2)
    {
        inches++;
    }

    int feet = inches / 12;
    inches -= feet * 12;
    cout << feet << " " << inches << endl;
}
