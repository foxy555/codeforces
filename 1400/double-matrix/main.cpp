#include <bits/stdc++.h>

#define ll long long

using namespace std;
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<vector<int>> a, b;
    for (int i = 0; i < n; i++) {
        vector<int> line(m);
        for (int j = 0; j < m; j++) {
            cin >> line[j];
        }
        a.push_back(line);
    }
    for (int i = 0; i < n; i++) {
        vector<int> line(m);
        for (int j = 0; j < m; j++) {
            cin >> line[j];
        }
        b.push_back(line);
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (a[i][j] < b[i][j]) {
                swap(a[i][j], b[i][j]);
            }
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (i != n - 1) {
                if (a[i][j] >= a[i + 1][j]) {
                    cout << "Impossible" << endl;
                    return 0;
                }
                if (b[i][j] >= b[i + 1][j]) {
                    cout << "Impossible" << endl;
                    return 0;
                }
            }
            if (j != m - 1) {
                if (a[i][j] >= a[i][j + 1]) {
                    cout << "Impossible" << endl;
                    return 0;
                }
                if (b[i][j] >= b[i][j + 1]) {
                    cout << "Impossible" << endl;
                    return 0;
                }
            }
        }
    }
    cout << "Possible" << endl;
}
