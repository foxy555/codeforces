#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    string s;
    cin >> n >> s;

    //vector<int> copynpaste;
    int ans = 0;
    for (int i = 1; i < n; i++)
    {
        if (i + i <= n)
        {
            if (s.substr(i, i) == s.substr(0, i))
            {
                ans = i - 1;
            }
        }
    }
    cout << n - ans << endl;
}