#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist(n);
    for (int i = 0; i < n; ++i) {
        cin >> nlist[i];
    }

    sort(nlist.begin(), nlist.end());
    vector<int> ans(n);
    int j = 0;
    for (int i = 1; i < n; i += 2) {
        ans[i] = nlist[j++];
    }
    for (int i = 0; i < n; i += 2) {
        ans[i] = nlist[j++];
    }
    int cnt = 0;
    for (int i = 1; i < n - 1; i += 2) {
        if (ans[i - 1] > ans[i] && ans[i + 1] > ans[i]) ++cnt;
    }
    cout << cnt << endl;
    for (auto i : ans) {
        cout << i << " ";
    }
}
