#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        ll a, b, n, m;
        cin >> a >> b >> n >> m;

        if (a + b < n + m)
        {
            cout << "No" << endl;
            continue;
        }

        if (min(a, b) >= m)
        {
            cout << "Yes";
        }
        else
            cout << "No";
        cout << endl;
    }
}
