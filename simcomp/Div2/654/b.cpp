#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        ll n, r;
        cin >> n >> r;
        ll ans = 1;
        if (r > 1)
        {
            ans += (min(n, r) - 1) * min(n, r) / 2 - 1;
            if (n <= r)
                ans++;
            else
                ans += min(n, r);
        }
        cout << ans << endl;
    }
}
