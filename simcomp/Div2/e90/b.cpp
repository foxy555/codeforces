#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;
        int zero, one;
        zero = one = 0;
        for (int i = 0; i < s.length(); ++i)
        {
            if (s[i] == '0')
                zero++;
            else
                one++;
        }
        if (min(zero, one) % 2)
            cout << "DA" << endl;
        else
            cout << "NET" << endl;
    }
}
