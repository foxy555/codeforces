#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        ll a, b, c;
        cin >> a >> b >> c;
        if (a < c)
        {
            cout << 1 << " ";
        }
        else
            cout << -1 << " ";
        if (c < a * b)
        {
            cout << b << endl;
        }
        else
            cout << -1 << endl;
    }
}
