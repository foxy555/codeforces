#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        ll a, b;
        cin >> a >> b;
        int ans = 0;
        bool flag = false;
        if (a > b)
        {
            while (a % 8 == 0 && a / 8 >= b)
            {
                if (a == b)
                {
                    cout << ans << endl;
                    flag = true;
                    break;
                }
                a /= 8;
                ans++;
            }
            while (!flag && a % 4 == 0 && a / 4 >= b)
            {

                if (a == b)
                {
                    cout << ans << endl;
                    flag = true;
                    break;
                }
                a /= 4;
                ans++;
            }
            while (!flag && a % 2 == 0 && a / 2 >= b)
            {

                if (a == b)
                {
                    cout << ans << endl;
                    flag = true;
                    break;
                }
                a /= 2;
                ans++;
            }
        }
        else
        {
            while (a * 8 <= b)
            {
                if (a == b)
                {
                    cout << ans << endl;
                    flag = true;
                    break;
                }
                a *= 8;
                ans++;
            }

            while (a * 4 <= b && !flag)
            {
                if (a == b)
                {
                    cout << ans << endl;
                    flag = true;
                    break;
                }
                a *= 4;
                ans++;
            }
            while (a * 2 <= b && !flag)
            {
                if (a == b)
                {
                    cout << ans << endl;
                    flag = true;
                    break;
                }
                a *= 2;
                ans++;
            }
        }
        if (a == b)
        {
            cout << ans << endl;
        }
        else
        {
            cout << -1 << endl;
        }
    }
}