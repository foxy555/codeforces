#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int a, b;
        cin >> a >> b;

        if (a * 2 > b)
        {
            cout << -1 << " " << -1 << endl;
        }
        else
            cout << a << " " << 2 * a << endl;
    }
}
