#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<pair<int, int>> nlist(n);
        int minc, mino;
        minc = mino = INT_MAX;
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i].first;
            minc = min(minc, nlist[i].first);
        }
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i].second;
            mino = min(mino, nlist[i].second);
        }
        ll ans = 0;
        for (int i = 0; i < n; ++i)
        {
            ans += min(nlist[i].first - minc, nlist[i].second - mino);
            int duo = min(nlist[i].first - minc, nlist[i].second - mino);
            nlist[i].first -= duo;
            nlist[i].second -= duo;
            ans += nlist[i].first - minc;
            ans += nlist[i].second - mino;
        }
        cout << ans << endl;
    }
}
