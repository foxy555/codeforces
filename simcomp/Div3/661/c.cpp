#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(100, 0);
        for (int i = 0; i < n; ++i)
        {
            int a;
            cin >> a;
            nlist[a]++;
        }
        int ans = 0;
        for (int i = 2; i <= 100; ++i)
        {
            vector<int> temp = nlist;
            int cnt = 0;
            for (int j = 1; j <= i / 2; ++j)
            {
                if (temp[j] && temp[i - j] && i - j != j)
                {
                    int duo = min(temp[j], temp[i - j]);
                    temp[j] -= duo;
                    temp[i - j] -= duo;
                    cnt += duo;
                }
                if (i - j == j)
                {
                    if (temp[j] - 2 >= 0)
                    {
                        cnt += temp[j] / 2;
                        temp[j] -= temp[j];
                    }
                }
            }
            ans = max(ans, cnt);
        }
        cout << ans << endl;
    }
}
