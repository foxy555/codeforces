#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        set<int> nset;
        for (int i = 0; i < n; ++i)
        {
            int a;
            cin >> a;
            nset.insert(a);
        }

        vector<int> nlist;
        for (auto i : nset)
        {
            nlist.push_back(i);
        }
        int begin = nlist.front();
        bool flag = 1;
        for (int i = 1; i < nlist.size(); ++i)
        {
            if (begin + 1 != nlist[i])
            {
                flag = 0;
                break;
            }
            else
                begin++;
        }
        if (flag)
        {
            cout << "YES" << endl;
        }
        else
            cout << "NO" << endl;
    }
}
