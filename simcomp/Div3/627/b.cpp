#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int len;
        cin >> len;

        vector<int> nlist(len);
        vector<int> p(len + 1), bp;
        vector<int> f(len + 1, -1);
        for (int i = 0; i < len; i++)
        {
            cin >> nlist[i];
            p[nlist[i] - 1]++;
            if (f[nlist[i] - 1] == -1)
                f[nlist[i] - 1] = i;
        }
        bp = p;
        sort(bp.rbegin(), bp.rend());
        if (bp[0] >= 3)
        {
            cout << "YES" << endl;
        }
        else
        {
            bool flag = false;
            for (int i = len - 1; i >= 0; i--)
            {
                if (f[nlist[i] - 1] + 1 < i)
                {
                    flag = true;
                    break;
                }
            }
            if (flag)
                cout << "YES" << endl;
            else
            {
                cout << "NO" << endl;
            }
        }
    }
}