#include "bits/stdc++.h"

#define ll long long

using namespace std;
vector<int> primelist;
void sieve(int n) {
    bool prime[n + 1];
    memset(prime, true, sizeof(prime));

    for (int p = 2; p * p <= n; p++) {
        if (prime[p] == true) {
            for (int i = p * 2; i <= n; i += p) prime[i] = false;
        }
    }

    for (int p = 2; p <= n; p++)
        if (prime[p]) primelist.emplace_back(p);
}
int primefactorcount = 0;
void countprimes(int n) {
    for (int i = 0; i < primelist.size(); ++i) {
        if (primelist[i] * primelist[i] > n) {
            break;
        }
        while (n % primelist[i] == 0) {
            n /= primelist[i];
            ++primefactorcount;
        }
    }
    if (n > 1) {
        ++primefactorcount;
    }
}
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    sieve(50000);
    int tc;
    cin >> tc;
    while (tc--) {
        primefactorcount = 0;
        int a, b, k;
        int acount = 0, bcount = 0;
        cin >> a >> b >> k;
        if (k == 1) {
            if (a == b) {
                cout << "NO" << endl;
                continue;
            }
            if (a % b == 0 || b % a == 0) {
                cout << "YES" << endl;
                continue;
            } else {
                cout << "NO" << endl;
                continue;
            }
        }

        vector<int> alist, blist;
        countprimes(a);
        countprimes(b);
        if (k > primefactorcount) {
            cout << "NO" << endl;
            continue;
        } else {
            cout << "YES" << endl;
        }
    }
}
